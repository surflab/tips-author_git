from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.ext import blobstore
import logging
import itertools

def listify(iterable):
  l = map(lambda k: "'%s'" % k, iterable)
  if len(l) == 0:
    return "nothing"
  elif len(l) == 1:
    return l[0]
  else:
    return ", ".join(l[:-1]) + " and " + l[-1]

class Organ(ndb.Model):
  """A template organ created by a user"""
  name = ndb.StringProperty()
  verbs = ndb.StringProperty(repeated=True)
  data = ndb.BlobKeyProperty(required=False)
  # m-n relationship should be implemented with a
  # third model. Repeated properties are inefficient
  # at cardinality larger than 100
  related_organs = ndb.KeyProperty(repeated=True)
  connected_related_organs = ndb.KeyProperty(repeated=True)
  variant_organs = ndb.KeyProperty(repeated=True)

  def brief(o):
    return { 'id': o.key.id(),
      'name': o.name,
      'verbs': o.verbs }

  def toJSON(o):
    retval = { 'id': o.key.id(),
      'name': o.name,
      'verbs': o.verbs,
      'related_organs': [ { 'id': k.id(), 'name': k.get().name } for k in o.related_organs ],
      'connected_related_organs': [ { 'id': k.id(), 'name': k.get().name } for k in o.connected_related_organs ],
      'variant_organs': [ { 'id': k.id(), 'name': k.get().name } for k in o.variant_organs ]
    }
    if o.data is not None:
      d = blobstore.get(o.data)
      retval['data'] = {  'filename': d.filename, 'size': d.size } # 'key': str(d.key()),
    return retval

  def fromJSON(o, j):
    o.name = j.get('name')
    o.verbs = j.get('verbs', [])
    o.related_organs = [ ndb.Key(Organ,r['id']) for r in j.get('related_organs', []) ]
    o.connected_related_organs = [ ndb.Key(Organ,r['id']) for r in j.get('connected_related_organs', []) ]
    o.variant_organs = [ ndb.Key(Organ,r['id']) for r in j.get('variant_organs', []) ]

class Tool(ndb.Model):
  """A template tool created by a user"""
  name = ndb.StringProperty()
  verbs = ndb.StringProperty(repeated=True)
  data = ndb.BlobKeyProperty(required=False)

  def brief(o):
    return { 'id': o.key.id(),
      'name': o.name,
      'verbs': o.verbs }

  def toJSON(o):
    retval = { 'id': o.key.id(),
      'name': o.name,
      'verbs': o.verbs,
    }
    if o.data is not None:
      d = blobstore.get(o.data)
      retval['data'] = {  'filename': d.filename, 'size': d.size } # 'key': repr(d.key()),
    return retval

  def fromJSON(o, j):
    o.name = j.get('name')
    o.verbs = j.get('verbs', [])


class User(ndb.Model):
  """User roles created by administrator"""
  name = ndb.StringProperty()
  #name = ndb.UserProperty()
  isAdmin = ndb.BooleanProperty()
  isAuthor = ndb.BooleanProperty()
  isArtist = ndb.BooleanProperty()

  def brief(o):
    return { 'id': o.key.id(),
      'name': o.name,
      'isAdmin': o.isAdmin,
      'isAuthor': o.isAuthor,
      'isArtist': o.isArtist }

  def toJSON(o):
    retval = { 'id': o.key.id(),
       'name': o.name,
       'isAdmin': o.isAdmin,
       'isAuthor': o.isAuthor,
       'isArtist': o.isArtist }
    return retval

  def fromJSON(o, j):
    o.name = j.get('name')
    o.isAdmin = j.get('isAdmin')
    o.isAuthor = j.get('isAuthor')
    o.isArtist = j.get('isArtist')


class Procedure(ndb.Model):
  name = ndb.StringProperty()
  data = ndb.BlobKeyProperty(required=False)
  author = ndb.UserProperty()
  stages = ndb.JsonProperty()
  comments = ndb.JsonProperty()
  # cameraTrocar = ndb.StringProperty()
  cameraSetup = ndb.JsonProperty()
  quiz = ndb.JsonProperty()

  def author_or_none(self):
    if self.author != None:
      return self.author.nickname()
    else:
      return None

  def toJSON(self):
    retval = { 'name': self.name,
      'stages': self.stages,
      'author': self.author_or_none(),
      'id' : self.key.id(),
      'cameraSetup' : self.cameraSetup,
      # 'cameraTrocar' : self.cameraTrocar,
      'quiz': self.quiz,
      'comments': self.comments
    }
    if self.data is not None and str(self.data) != 'undefined':
        d = blobstore.get(self.data)
        logging.info("Data : %s " % self.data)
        retval['data'] = {  'filename': d.filename, 'size': d.size } # 'key': repr(d.key()),
    return retval

  def brief(self):
    return { 'name': self.name,
      'author': self.author_or_none(),
      'id' : self.key.id(),
      'quiz': self.quiz
    }

  def fromJSON(self,j):
    self.name = j['name']
    if 'stages' in j: self.stages = j['stages']
    if 'cameraSetup' in j: self.cameraSetup = j['cameraSetup']
    # if 'cameraTrocar' in j: self.cameraTrocar = j['cameraTrocar']
    if self.author is None:
      self.author = users.get_current_user()
    if 'quiz' in j: self.quiz = j['quiz']
    if 'comments' in j: self.comments = j['comments']

  # kind is the class
  def objects_used(self, Obj):
    kind = Obj._get_kind().lower()
    oo = {}
    for stage in self.stages:
      for task in stage['tasks']:
        o = task[kind]
        v = task['verb']
        if o in oo:
          if type(v) is dict:
            oo[o]['verbs'].add(v['name'])
          else:
            oo[o]['verbs'].add(v) 
        else:
          if type(v) is dict:
            oo[o] =  { 'verbs' : set([v['name']]), 'candidate': None }
          else:
            oo[o] =  { 'verbs' : set([v]), 'candidate': None }

    # Now let's try to find candidates
    for o in oo:
      q = Obj.query(Obj.name == o,
        ndb.AND(*map(lambda v: Obj.verbs == v, oo[o]['verbs'])))
      obj = q.get()
      oo[o]['candidate'] = obj
      if Obj == Organ and obj != None:
          oo[o]['related'] = [ r.get() for r in itertools.chain(obj.related_organs,obj.connected_related_organs) ]
          oo[o]['connected_related'] = [ r.get() for r in obj.connected_related_organs ]
          oo[o]['just_related'] = [ r.get() for r in obj.related_organs ]
          oo[o]['variant'] = [ r.get() for r in obj.variant_organs ]

    return oo

  @classmethod
  def convert_object_list_to_json(oo):
    if oo[o]['candidate'] != None:
      oo[o]['candidate'] = oo[o]['candidate'].key.id()

    if 'related' in oo[o] and oo[o]['related'] != None:
      oo[o]['related'] = [ r.key.id() for r in oo[o]['related'] ]

    oo[o]['verbs'] = list(oo[o]['verbs'])

  # Return a mapping of organ names used
  # to the appropriate candidate of Organ entities
  # by looking through the stages and
  # finding the right Organ
  def organs(self):
    return self.objects_used(Organ)

  def tools(self):
    return self.objects_used(Tool)

  def organs_and_tools(self):
    # Figure out the list of objects that are needed
    # We have to collect all the tools and unique them
    # Collect all the organs and verbs associated with them
    organs = set()
    tools = set()
    for stage in self.stages:
      for task in stage['tasks']:
        o = task['organ']
        v = task['verb']
        if type(v) is dict:
          v = task['verb']['name']
        if o in organs:
          organs[o].add(v)
        else:
          organs[o] = set([v])
        tools.add(task['tool'])
    return organs, tools

class Quiz(ndb.Model):
  """Quizzes created by author or administrator"""
  name = ndb.StringProperty()
  url = ndb.StringProperty()

  def brief(o):
    return { 'id': o.key.id(),
        'name': o.name,
        'url': o.url }

  def toJSON(o):
    retval = { 'id': o.key.id(),
        'name': o.name,
        'url': o.url }
    return retval

  def fromJSON(o, j):
    o.name = j.get('name')
    o.url = j.get('url')
