#!/usr/bin/env python
import webapp2
import json
import urllib

from google.appengine.api import users
from google.appengine.ext import blobstore
from google.appengine.ext.blobstore import BlobInfo
from google.appengine.ext.webapp import blobstore_handlers

from models import *
from genscene import generate_scene_file, LogicalError
from preview import generate_scene_preview, generate_object_preview

import os
from zipfile import ZipFile
import logging

app = webapp2.WSGIApplication(debug=False)

def route(*args, **kwargs):
    def wrapper(func):
        app.router.add(webapp2.Route(handler=func, *args, **kwargs))
        return func
    return wrapper

def json_success(request, data):
  request.response.content_type = 'application/json'
  request.response.write(json.dumps(data))

def json_error(request, code, errstr):
  request.response.content_type = 'application/json'
  request.response.write(errstr)
  request.response.status = code

## Maximum number of entities fetched in one query
FETCH_LIMIT = 100

def rest_model(Model):
  model_lower = Model.__name__.lower()
  preURL = '/api/%ss/' % model_lower
  if model_lower == 'quiz':
    preURL = '/api/quizzes/'

  @route(preURL, methods=['GET'])
  def model_list(request):
    return json_success(request, map(lambda m: m.brief(), Model.query().fetch(FETCH_LIMIT)))

  @route(preURL + '<id:\d+>', methods=['GET'])
  def model_get(request, id):
    k = ndb.Key(Model, int(id))
    m = k.get()
    if(m is not None):
      return json_success(request,  m.toJSON())
    else:
      return json_error(request, 404, 'entity not found')

  @route(preURL + '<id:\d+>', methods=['DELETE'])
  def model_delete(request, id):
    k = ndb.Key(Model, int(id)) # Key of the model in the DataStore
    m = k.get()
    if hasattr(m, 'data') and m.data is not None:
      blobstore.delete(m.data)
    k.delete()
    return json_success(request,  'success')

  @route(preURL + '<id:\d+>', methods=['PUT'])
  def model_update(request, id):
    k = ndb.Key(Model, int(id))
    m = k.get()
    if(m is not None):
      action = 'updated'
    else:
      action = 'created'
      m = Model()

    m.fromJSON(json.loads(request.body))
    m.put()
    return json_success(request, { 'action' : action, 'id' : m.key.id() })

# REST APIs
rest_model(Procedure)
rest_model(Organ)
rest_model(Tool)
rest_model(User)
rest_model(Quiz)

# Download/Upload APIs
@route('/api/data/<model:organ|tool|procedure>/<id:\d+>', methods=['GET'])
class ModelDownload(blobstore_handlers.BlobstoreDownloadHandler):
  def get(self, model, id):
    k = ndb.Key(model.title(), int(id))
    m = k.get()
    if m is None or m.data is None or blobstore.get(m.data) is None:
      self.response.write('The model does not have an associated data file')
      self.response.status = 404
    else:
      d = blobstore.get(m.data)
      self.send_blob(d, save_as=d.filename)

@route('/api/data_preview/<model:organ|tool>/<id:\d+>', methods=['GET'])
class ModelDownload(blobstore_handlers.BlobstoreDownloadHandler):
  def get(self, model, id):
    k = ndb.Key(model.title(), int(id))
    m = k.get()
    if m is None or m.data is None or blobstore.get(m.data) is None:
      return json_error(self.request, 404, 'The model does not have an associated data file')
    else:
      d = blobstore.get(m.data)
      r = blobstore.BlobReader(m.data)
      if(str(d.content_type) == "text/xml" ):
          self.response.headers['Content-Type'] = 'text/xml'
          text = r.read()
          self.response.write(generate_object_preview(text))
      else:
        z = ZipFile(r, 'r')
        f = [ d for d in z.namelist() if '.xml' in d][0]
        objArray = [d for d in z.namelist() if '.obj' in d]
        mtlArray = [ d for d in z.namelist() if '.mtl' in d]
        if f:
          self.response.headers['Content-Type'] = 'text/xml'
          if objArray:
            self.response.write(generate_object_preview(z.open(f).read(), z.open(objArray[0]).read(), z.open(mtlArray[0]).read()))
          else:
            self.response.write(generate_object_preview(z.open(f).read()))
          z.close()
        else:
          return json_error(self.request, 404, 'The model does not have an associated data file')

@route('/api/data/<model:organ|tool|procedure>/<id:\d+>', methods=['POST'])
class ModelUpload(blobstore_handlers.BlobstoreUploadHandler):
  def post(self, model, id):
    k = ndb.Key(model.title(), int(id))
    m = k.get()
    files = self.get_uploads()
    if m is None:
      self.response.write('Model not found')
      self.response.status = 404
    elif len(files) == 0:
      return json_error(self.request, 400, 'No file was uploaded')
    else:
      m.data = files[0].key()
      terminate = False

      f = blobstore.get(files[0].key())
      _, ext = os.path.splitext(f.filename)
      if ext.lower() == '.zip':
        r = blobstore.BlobReader(m.data)
        z = ZipFile(r, 'r')
        exts = [ os.path.splitext(f)[1] for f in z.namelist() ] # list of file extensions contained within the .zip
        logging.info(exts)
        if not ('.blend' in exts and '.xml' in exts):
          terminate = True

      if terminate:
        error = 'Selected .zip archive should contain exactly 2 files (one .xml and one .blend). \nInstead it contains: ' + ', '.join(exts)
        error = 'Selected .zip archive should have .xml and .blend files. Additionally, only .obj or .mtl file types are supported.'
        return json_error(self.request, 406, error)
      else:
        m.put()
        return json_success(self.request, 'success')


@route('/api/data/<model:organ|tool|procedure>/<id:\d+>/upload', methods=['GET'])
def model_upload_request(request, model, id):
  k = ndb.Key(model.title(), int(id)) # Key of the model in the DataStore
  m = k.get()
  if m is None:
    return json_error(request, 404, 'model not found')
  else:
    return json_success(request, blobstore.create_upload_url('/api/data/' + model + '/' + id))

# Other API endpoints

@route('/api/genscene/<procedure_id:\d+>/<position:>/<orientation:>/<initInfo:>', methods=['GET'])
def generate_scene_for_procedure(request, procedure_id, position, orientation, initInfo):
  k = ndb.Key(Procedure, int(procedure_id))
  p = k.get()
  try:
    s = generate_scene_file(p,position, orientation, initInfo)
    request.response.content_type = 'application/zip'
    request.response.headers.add('Content-Disposition','attachment;filename=%s.zip' % str(urllib.quote(p.name)))
    request.response.write(s.getvalue())
    s.close()
  except LogicalError as e:
    request.response.content_type = 'text/plain'
    request.response.write('Error while generating scene for "%s":\n\n%s' % (p.name, e.args[0]))


@route('/api/genpreview/<procedure_id:\d+>', methods=['GET'])
def generate_preview_for_procedure(request, procedure_id):
  k = ndb.Key(Procedure, int(procedure_id))
  p = k.get()
  try:
    s = generate_scene_preview(p)
    request.response.headers['Content-Type'] = 'text/xml'
    logging.info(request.response.content_type)
    request.response.write(s)
    #s.close()
  except LogicalError as e:
    request.response.content_type = 'text/plain'
    request.response.write('Error while generating scene for "%s":\n\n%s' % (p.name, e.args[0]))

@route('/api/getOrgVariations/<organ_name>', methods=['GET'])
def get_organ_variations(request, organ_name):
  """Returns a list of organs that partially match the given organ name"""
  matchingOrgans = list()
  try:
    for o in Organ.query().fetch(FETCH_LIMIT):
      if organ_name in o.name:
        matchingOrgans.append(o.name)
  except LogicalError as e:
    request.response.content_type = 'text/plain'
    request.response.write('Error while generating a list of realated organs: \n%s' % (e.args[0]))
  return json_success(request, {'organs': matchingOrgans})

@route('/api/getToolVariations/<tool_name>', methods=['GET'])
def get_organ_variations(request, tool_name):
  """Returns a list of organs that partially match the given organ name"""
  matchingTools = list()
  try:
    for t in Tool.query().fetch(FETCH_LIMIT):
      if tool_name in t.name:
        matchingTools.append(t.name)
  except LogicalError as e:
    request.response.content_type = 'text/plain'
    request.response.write('Error while generating a list of realated tools: \n%s' % (e.args[0]))
  return json_success(request, {'tools': matchingTools})

@route('/api/genstatus/<procedure_id:\d+>')
def generate_scene_status(request, procedure_id):
  procedure = Procedure.get_by_id(int(procedure_id))
  if procedure == None:
    return json_error(request, 404, 'procedure not found')

  generatable = 1
  organs = procedure.objects_used(Organ)
  tools = procedure.objects_used(Tool)

  # at this stage we might throw error if something does
  # not have a candidate
  organs_json = []
  for o in organs:
    if organs[o]['candidate'] == None:
      generatable = 0
      c = None
    else:
      c = organs[o]['candidate'].toJSON()
    organs_json.append( { 'name': o, 'verbs': list(organs[o]['verbs']), 'candidate': c } )
  tools_json = []
  for t in tools:
    if tools[t]['candidate'] == None:
      generatable = 0
      c = None
    else:
      c = tools[t]['candidate'].toJSON()
    tools_json.append( { 'name': t, 'verbs': list(tools[t]['verbs']), 'candidate': c } )

  organ_name = None
  variant_name = None
  for o in organs:
    if 'variant' in organs[o] and len(organs[o]['variant']) > 0 and organs[o]['variant'][0].name in organs:
      generatable = 2
      organ_name = o
      variant_name = organs[o]['variant'][0].name
  
  connected_organ_candidates = []
  connection = []
  for o in organs:
    if 'connected_related' in organs[o] and len(organs[o]['connected_related']) > 0:
      connection.append(o)
      connected_organ_candidates.append(organs[o]['connected_related'][0].name)

  for o in organs:
    if 'variant' in organs[o] and len(organs[o]['variant']) > 0 and organs[o]['variant'][0].name in connected_organ_candidates:
      generatable = 3
      organ_name = o
      index = connected_organ_candidates.index(organs[o]['variant'][0].name)
      variant_name = connection[index]
      

  return json_success(request, { 'generatable' : generatable, 'organs': organs_json, 'tools': tools_json, 'organ_name': organ_name, 'variant_name': variant_name})

@route('/api/existingHeaderfiles', methods=['GET'])
def get_files_list(request):
  """Returns all of the unique headerfiles"""
  headers = list()
  for f in BlobInfo.all().fetch(FETCH_LIMIT):
    if f.filename.endswith(".scn") and f not in headers:
      headers.append({'filename' : f.filename, 'blobkey' : f.key().ToXml()})
  return json_success(request, headers)

@route('/api/selectExistingHeaderfile/<procedure_id:\d+>/<blobkey:>', methods=['PUT'])
def put_header_file(request, procedure_id, blobkey):
  """Replaces the blobkey of the procedure with the blobkey of another exising file"""
  procedure = Procedure.get_by_id(int(procedure_id))
  procedure.data = blobstore.BlobKey(blobkey)
  procedure.put()
  return request.response.set_status(200)

@route('/api/autocomplete', methods=['GET'])
def get_names_list(request):
  """Resturns all of the organs, tools and verbs"""
  verbs = set()
  organs = []
  tools = []
  for o in Organ.query().fetch(FETCH_LIMIT):
    for v in o.verbs:
      verbs.add(v)
    organs.append({ 'name': o.name, 'id': o.key.id() , 'verbs': o.verbs})
  for t in Tool.query().fetch(FETCH_LIMIT):
    for v in t.verbs:
      verbs.add(v)
    tools.append({ 'name': t.name, 'id': o.key.id() , 'verbs': t.verbs})
  verbs.discard('')
  topDownVerbs = []
  for v in verbs:
    verbOrgans = []
    verbTools = []
    for o in organs:
      if o['id'] not in verbOrgans and v in o['verbs']:
        verbOrgans.append(o)
    for t in tools:
      if t['id'] not in verbTools and v in t['verbs']:
        verbTools.append(t)
    topDownVerbs.append({ 'name': v, 'organs': verbOrgans, 'tools': verbTools})
  return json_success(request, { 'verbs' : list(topDownVerbs), 'organ': organs, 'tool': tools })
  # return json_success(request, { 'verbs' : list(topDownVerbs)})

@route('/api/auth')
def auth_get(request):
  """Authentication data that is used for the right side of the navigation bar"""
  user_list = []
  for u in User.query().fetch(FETCH_LIMIT):
    user_list.append(u)

  u = users.get_current_user()
  if u != None:
    matches = [x for x in user_list if x.name == u.nickname()] # try to find user in database
    if len(matches) > 0:
      r = { 'name' : u.nickname(), 'logout': users.create_logout_url('/'), 'isAdmin': matches[0].isAdmin, 'isArtist': matches[0].isArtist,'isAuthor': matches[0].isAuthor}
    else: # new user
      if u.nickname() == "surflab.cise": # DEFAULT ADMIN!!
        User(name=u.nickname(),isAdmin=True).put() # add new login to user database
        r = { 'name' : u.nickname(), 'logout': users.create_logout_url('/'), 'isAdmin': True}
      else:
        User(name=u.nickname()).put() # add new login to user database
        r = { 'name' : u.nickname(), 'logout': users.create_logout_url('/')}
  else:
    r = { 'name' : None, 'login': users.create_login_url('/') }
  return json_success(request, r)
