from models import *
from google.appengine.ext import ndb, blobstore
from zipfile import ZipFile
from io import BytesIO
import json
import xml.etree.ElementTree as ET
import os
from math import sqrt, acos, sin, cos, pi
from google.appengine.api import app_identity

import logging
# Note: We do not have protection against
# Zip bombs and XML bombs, for XML one can use
# defusedxml package

def find_node_by_name(e, name):
  for n in e.iterfind("Node"):
    if n.get('name') == name:
      return n
    x = find_node_by_name(n, name)
    if x is not None: return x

  return None

def iterable(o):
  return hasattr(o, '__getitem__') and hasattr(o, '__len__')

def vector_to_string(v):
  t = ""
  for i in v :
    if iterable(i):
      t += vector_to_string(i) + ' '
    else:
      t += str(i) + ' '
  return t

def angle_axis(angle,axis):
  s, c = sin(angle/2), cos(angle/2)
  return [ axis[0] * s, axis[1] * s, axis[2] * s, c ]

def dot(p, q):
  a,b,c,d = p
  e,f,g,h = q
  return [ d*e + h*a + b*g - c*f, d*f + h*b - a*f + c*e, d*g + h*c + a*f - b*e, d*h - a*e - b*f - c*g ]

def generateCamera(position, orientation):
  '''
  # The Z coordinate of the top of the torso in the picture used in UI in centimeters
  topZ = 151
  # The Z coordinate of the bottom of the torso
  bottomZ = 97
  # Distance of the camera from the Z axis (spine) centimeters
  radius = 10
  # the ratio of the torsoWidth to the width of the image
  torsoWidth = 0.8

  a = float(trocar['y'])
  z = (1 - a) * topZ + a * bottomZ;
  # Find the sine of the angle, then cosine
  s = (float(trocar['x']) - 0.5) * 2 / torsoWidth
  # find the angle
  angle = acos(s)
  position = [ radius * cos(angle), - radius * sin(angle), z ]
  orientation = dot(angle_axis(pi/2 - angle, [0, 0, 1]), angle_axis(pi/2, [1, 0, 0]))
  print(angle,position,orientation)
  '''
  
  return ET.Element('InteractiveCamera', position=position, orientation=orientation, distance="10")

def create_toplevel_xml(procedure,organs_fn,obstacle_organs_fn, connected_organ_fn, tools,position,orientation, initInfo):

  def weightedSum(one, two):
    return (one+two)/2.0

  doc = ET.parse('xml/template.scn')
  root = doc.getroot()
  if procedure.data != None:
      r = blobstore.BlobReader(procedure.data)
      root = ET.fromstring(r.read())
  else:
      root = ET.parse('xml/UniversalHeader.scn').getroot()
  toolMap = { 
              "Right_angled_Hook_Suction_Electrode_Coagulators": "tRight_angled_Hook_Suction_Electrode_Coagulators_",
              "Blunt Grasper": "tBlunt_grasper",
              "Camera Tool": "CameraTool",
              "Curved Maryland Dissector": "tCurved Maryland Dissector open", 
              "Clip Applier": "tClipApplier",
              "Curved Scissor": "tCurved Scissor open", 
              "Duet TRS": "tDuet TRS close", 
              "Retractor": "tRetractor_grasp",
              "Pouch large (for gallbladder)": "tTIPS_Pouch_for_Gallbladder",
              "Pouch small (for appendix)": "tTIPS_Pouch_small",
              "Suture Needle": "tBlunt_grasper_wNeedle"
            }
      
  if position != None:
    if root.find('InteractiveCamera') == None:
      root.append(generateCamera(position,orientation))
    sh=root.find('InteractiveCamera')
    sh.set('position', position)
    sh.set('orientation', orientation.replace(',',' ', 3))

  hapDeviceList = root.findall('*/*/SurfLabHapticDevice')
  
  initArray = [float(x) for x in initInfo.split(' ')][0:3]
  positionArray = [float(x) for x in position.split(' ')]
  resArray = list(map(weightedSum, initArray, positionArray))
  for hapDevice in hapDeviceList:
    hapDevice.set('orientationBase', orientation.replace(',',' ', 3))
    hapDevice.set('positionBase', ' '.join(str(e) for e in resArray))
    hapDevice.set('desirePosition', ' '.join(str(e) for e in positionArray))

  for hname in ['Haptic1', 'Haptic2']:
    h  = find_node_by_name(root, 'Instrument__' + hname)
    toolsName = []
    for tool in tools:
      toolsName.append(toolMap[tool])

    model = root.getiterator("Node")
    for item in model: 
      if item.get('name') in ['Instruments_of_PHANToM 1', 'Instruments_of_PHANToM 2']:
        for i in item.getiterator("Node"):
          if i.get('tags') is not None and i.get('tags').lower() == "instrument" and i.get('name') not in toolsName:
            item.remove(i)

  # insert organs into xml
  s = find_node_by_name(root, 'SolverNode')
  start_idx = s.getchildren().index(s.find('CGLinearSolver')) + 1
  sorted_organs = sorted([ (attrib.get('author-parent','root'), float(attrib.get('author-order',10000000)), fn) for fn,attrib in organs_fn ])
  for j, (parent,i,fn) in enumerate(sorted_organs):
    r = root.find("*[@name='%s']" % parent) or s
    r.insert(start_idx+j,ET.Element('include', href = fn))
    #r.append(ET.Element('include', href = fn))

  # insert connected related organs into xml
  s = find_node_by_name(root, 'SolverNode')
  start_idx = s.getchildren().index(s.find('CGLinearSolver')) + 1
  sorted_connected_organs = sorted([ (attrib.get('author-parent','root'), float(attrib.get('author-order',10000000)), fn) for fn,attrib in connected_organ_fn ])

  for j, (parent,i,fn) in enumerate(sorted_organs): 
    for j1, (parent1,i1,fn1) in enumerate(sorted_connected_organs): 
      if fn == fn1:
        del sorted_connected_organs[j1]

  for j, (parent,i,fn) in enumerate(sorted_connected_organs):
    r = root.find("*[@name='%s']" % parent) or s
    r.insert(start_idx+j,ET.Element('include', href = fn))

  # insert obstacle (related) organs into xml
  start_idx = root.getchildren().index(s)
  sorted_obst_organs = sorted([ (attrib.get('author-parent','root'), float(attrib.get('author-order',10000000)), fn) for fn,attrib in obstacle_organs_fn ])
  for j, (parent,i,fn) in enumerate(sorted_obst_organs):
    root.insert(start_idx+j,ET.Element('include', href = fn))

  return ET.tostring(root)

class LogicalError(Exception):
  def __init__(self, msg):
    Exception.__init__(self, msg)

def generate_scene_file(procedure,position,orientation, initInfo):
  # First parse the procedure and get a list
  # of organs and tools for the procedure
  # these should be methods of Procedure itself
  # Find the right candidate for all organs and tools
  organs = procedure.objects_used(Organ)
  tools = procedure.objects_used(Tool)

  # at this stage we might throw error if something does
  # not have a candidate
  for o in organs:
    if organs[o]['candidate'] == None:
      raise LogicalError("Organ '%s' does not have a candidate that supports %s" % (o,listify(organs[o]['verbs'])))
  for t in tools:
    if tools[t]['candidate'] == None:
      raise LogicalError("Tool '%s' does not have a candidate that supports %s" % (t,listify(tools[t]['verbs'])))

  s = BytesIO()
  z = ZipFile(s, 'w')

  organ_candidates = []
  obstacle_organ_candidates = []
  connected_organ_candidates = []
  safety = []
  targetOrgan = []

  def add_candidate_data(c):
    d = blobstore.get(c.data)
    bn, ext = os.path.splitext(d.filename)
    dn = bn  + '-' + str(c.key.id())
    if ext.lower() == '.xml':
      fn = dn + '.xml'
      r = blobstore.BlobReader(d.key())
      # write the file into the Zip
      text = r.read()
    elif ext.lower() == '.zip':
      r = blobstore.BlobReader(d.key())
      # unzip it and add all the files
      # to the current ZIP in a new directory called dn
      zz = ZipFile(r, 'r')
      for f in zz.namelist():
        _, ext = os.path.splitext(f)
        if ext.lower() == '.xml':
            fn = dn + '/' + f
            text = zz.read(f)
      zz.close()

    else:
      raise LogicalError("Unknown file extension '%s'" % d.filename)

    n = ET.fromstring(text)
    for s in safety:
      if s['organ'] in c.name and '.xml' in fn:
        tag = ''
        if s['verb'] == 'not too close to':
          tag = ' SafetySurface'
        else:
          tag = ' SafetyForceThresold_4.0'
        
        triangleModel = n.find('*/*/TriangleModel')
        alternateTriangleModel = n.find("*/TriangleModel")
        if triangleModel is not None and tag not in triangleModel.get('tags'):
          triangleModel.set('tags', triangleModel.get('tags') + tag)
        elif alternateTriangleModel is not None and tag not in alternateTriangleModel.get('tags'):
          alternateTriangleModel.set('tags', alternateTriangleModel.get('tags') + tag)
        else:
          logging.error("Didn't find triangle model for organ file: " + fn)
    
    if c.name in targetOrgan:
      triangleModel = n.find('*/*/TriangleModel')
      alternateTriangleModel = n.find("*/TriangleModel")
      tag = "TargetOrgan"
      if triangleModel is not None and tag not in triangleModel.get('tags'):
        triangleModel.set('tags', triangleModel.get('tags') + " " +  tag)
      elif alternateTriangleModel is not None and tag not in alternateTriangleModel.get('tags'):
        alternateTriangleModel.set('tags', alternateTriangleModel.get('tags') + " " + tag)
      else:
        logging.error("Didn't find triangle model for organ file: " + fn)

    z.writestr(fn, ET.tostring(n))
    return fn, n.attrib

  for organ in organs.values():
    organ_candidates.append(organ['candidate'])
    obstacle_organ_candidates.extend(organ['just_related'])
    connected_organ_candidates.extend(organ['connected_related'])
  tool_candidates = [ tool['candidate'] for tool in tools.values() ]

  # distinct the objects
  procedureJson = procedure.toJSON()

  for stage in procedureJson['stages']:
    for task in stage['tasks']:
      if 'safety_organ' in task and 'safety_verb' in task:
        safety.append({'organ': task['safety_organ'], 'verb': task['safety_verb']})
      if 'verb' in task and 'name' in task['verb'] and task['verb']['name'] == 'place':
        targetOrgan.append(task['organ'])

  organs_keymap = dict([ (o.key, o) for o in organ_candidates ])
  obstacle_organs_keymap = dict([ (o.key, o) for o in obstacle_organ_candidates ])
  connected_organ_keymap = dict([ (o.key, o) for o in connected_organ_candidates ])
  tools = list([o.name for o in tool_candidates])
  # Unzip or include every objects into the main ZIP
  organ_filenames = map(add_candidate_data, organs_keymap.values())
  obstacle_organ_filenames = map(add_candidate_data, obstacle_organs_keymap.values())
  connected_organ_filenames = map(add_candidate_data, connected_organ_keymap.values())

  # Create the ZIP file
  z.writestr('source.json', json.dumps(procedureJson))
  z.writestr('index.scn', create_toplevel_xml(procedure,organ_filenames, obstacle_organ_filenames, connected_organ_filenames, tools, position,orientation, initInfo))
  logging.info('bucket name %s', str(os.environ.get('BUCKET_NAME', app_identity.get_default_gcs_bucket_name())))

  return s