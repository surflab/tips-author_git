/* GLOBAL VARIABLES */
// WebGL
var canvas;
var gl;
var program;
var vertexShader;
var fragmentShader;
var debugMode;
// Objects
var obj_vertices_buffer = [];
var obj_indices_buffer = [];
var obj_indices_count = [];
var obj_normals_buffer = [];
var obj_material_uniform = [];
var obj_u_M = [];


// Attributes & Uniforms
var v_position_attr;
var v_normal_attr;
var v_color_attr;
var u_P;
var u_V;
var u_M;
var u_M_Inv;
var u_light;
var u_eye;


// Camera
var center;
var camera;
var aspect_ratio;

// User Interaction
var last_x;
var last_y;

function createObject(vertices, indices, normals, material, scale, rotate, translate){
  buff_index = obj_vertices_buffer.length;

  // create vertices
  obj_vertices_buffer.push(gl.createBuffer());
  gl.bindBuffer(gl.ARRAY_BUFFER, obj_vertices_buffer[buff_index]);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  // create indices
  obj_indices_buffer.push(gl.createBuffer());
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj_indices_buffer[buff_index]);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
  obj_indices_count.push(indices.length);

  // create normals
  obj_normals_buffer.push(gl.createBuffer());
  gl.bindBuffer(gl.ARRAY_BUFFER, obj_normals_buffer[buff_index]);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);

  // add material
  obj_material_uniform.push(material);

  // create pivot
  var pivot = m4x4.eye();
  var scale_m = m4x4.scale(scale);
  var rotate_m = m4x4.rotate(rotate);
  var translate_m = m4x4.translate(translate);
  //The following order seems reversed is because everything is transposed here
  pivot = m4x4.multiply(translate_m, pivot);
  pivot = m4x4.multiply(rotate_m, pivot);
  pivot = m4x4.multiply(scale_m, pivot);
  
  obj_u_M.push(pivot);
}

function clearContext(){
  // unbind buffers
  for(var i = 0; i != obj_vertices_buffer.length; ++i){
    gl.bindBuffer(gl.ARRAY_BUFFER, obj_vertices_buffer[i]);
    gl.bufferData(gl.ARRAY_BUFFER, 1, gl.STATIC_DRAW);
    gl.deleteBuffer(obj_vertices_buffer[i]);

    gl.bindBuffer(gl.ARRAY_BUFFER, obj_normals_buffer[i]);
    gl.bufferData(gl.ARRAY_BUFFER, 1, gl.STATIC_DRAW);
    gl.deleteBuffer(obj_normals_buffer[i]);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj_indices_buffer[i]);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, 1, gl.STATIC_DRAW);
    gl.deleteBuffer(obj_indices_buffer[i]);
  }

  // clear arrays
  obj_vertices_buffer = [];
  obj_indices_buffer = [];
  obj_indices_count = [];
  obj_normals_buffer = [];
  obj_material_uniform = [];
  obj_u_M = [];


  // clear variables
  v_position_attr = null;
  v_normal_attr = null;
  v_color_attr = null;
  u_P = null;
  u_V = null;
  u_M = null;
  u_M_Inv = null;
  u_light = null;
  u_eye = null;

  camera = null;
  center = null;

  // clear shaders=
  gl.detachShader(program, vertexShader);
  gl.detachShader(program, fragmentShader);
  gl.deleteShader(vertexShader);
  gl.deleteShader(fragmentShader);
  gl.deleteProgram(program);
}

function drawAxes(){

  var vertices = [0,0,0,2,0,0,
  0,0,0,0,2,0,
  0,0,0,0,0,2];
  var indices = [0,1,2,3];
  var colors = [ 1,0,0,1,0,0, 0,1,0,0,1,0, 0,0,1,0,0,1]; //R G B for X Y Z axis

  // bind vertices
  var axes_vertices_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, axes_vertices_buffer);
  gl.vertexAttribPointer(v_position_attr, 3, gl.FLOAT, false, 0, 0);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  //bind indices
  var Index_Buffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, Index_Buffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  // bind colors
 var color_buffer = gl.createBuffer ();
 gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
 gl.vertexAttribPointer(v_color_attr, 3, gl.FLOAT, false, 0,0) ;
 gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
 gl.bindBuffer(gl.ARRAY_BUFFER, null);

 // set model matrix to be identity
 gl.uniformMatrix4fv(u_M, false, new Float32Array(m4x4.flatten(m4x4.eye())));
 gl.drawArrays(gl.LINES,0,6);  
    

}

function drawObject(index){

  // bind vertices
  gl.bindBuffer(gl.ARRAY_BUFFER, obj_vertices_buffer[index]);
  gl.vertexAttribPointer(v_position_attr, 3, gl.FLOAT, false, 0, 0);

  // bind normals
  gl.bindBuffer(gl.ARRAY_BUFFER, obj_normals_buffer[index]);
  gl.vertexAttribPointer(v_normal_attr, 3, gl.FLOAT, false, 0, 0);

  // set material uniform
  var u_diffuse = gl.getUniformLocation(program, "u_diffuse");
  gl.uniform4fv(u_diffuse, new Float32Array(obj_material_uniform[index].diffuse));
  var u_ambient = gl.getUniformLocation(program, "u_ambient");
  gl.uniform4fv(u_ambient, new Float32Array(obj_material_uniform[index].ambient));
  var u_specular = gl.getUniformLocation(program, "u_specular");
  gl.uniform4fv(u_specular, new Float32Array(obj_material_uniform[index].specular));
  var u_emissive = gl.getUniformLocation(program, "u_emissive");
  gl.uniform4fv(u_emissive, new Float32Array(obj_material_uniform[index].emissive));
  var u_shininess = gl.getUniformLocation(program, "u_shininess");
  gl.uniform1i(u_shininess, obj_material_uniform[index].shininess);

  // bind indices
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj_indices_buffer[index]);

  // set model uniform
  gl.uniformMatrix4fv(u_M, false, new Float32Array(m4x4.flatten(obj_u_M[index])));
  gl.uniformMatrix4fv(u_M_Inv, false, new Float32Array(m4x4.flatten(m4x4.transpose(m4x4.invert(obj_u_M[index])))));

  // draw
  gl.drawElements(gl.TRIANGLES, obj_indices_count[index], gl.UNSIGNED_SHORT, 0);
}


function init(input, cameraInitInfo){
  if(gl != undefined){
    clearContext();
  }
  debugMode = false;
  canvas = document.getElementById('c');
  gl = canvas.getContext('webgl');

  gl.clearColor(1.0, 1.0, 1.0, 1.0);
  gl.clearDepth(1.0);
  gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);
  gl.depthFunc(gl.LESS);
  //gl.enable(gl.CULL_FACE);
  //gl.cullFace(gl.BACK);

  var vertexShaderSource = `
  attribute vec3 a_v_position;
  attribute vec3 a_v_normal;
  attribute vec3 a_v_color;
  varying vec3 vColor;

  uniform mat4 u_M;
  uniform mat4 u_M_Inv;
  uniform mat4 u_V;
  //uniform mat4 u_V_Inv;
  uniform mat4 u_P;
  uniform vec3 u_light;
  uniform vec3 u_eye;

  varying vec3 v_normal;
  varying vec3 eye;
  varying vec3 light;
  void main(void) {
    gl_Position = u_P * (u_V * (u_M * vec4(a_v_position, 1.0)));
    vColor = a_v_color;

    // Lighting 
    // compute vpos, vnormal,lightdir, eyedir in world coord system
    v_normal = ( mat3(u_M_Inv) * a_v_normal ).xyz;
    vec3 world_position = (u_M * vec4(a_v_position, 1.0)).xyz;
    light = u_light - world_position;
    eye = u_eye - world_position;
  }`;

  var fragmentShaderSource = `
  precision mediump float;
  varying vec3 v_normal;
  varying vec3 light;
  varying vec3 eye;
  varying vec3 vColor;
  uniform vec4 u_ambient;
  uniform vec4 u_diffuse;
  uniform vec4 u_specular;
  uniform vec4 u_emission;
  uniform int u_shininess;


  void main(void) {
    vec3 normal = normalize(v_normal);

    vec3 light_dir = normalize(light);
    vec3 eye_dir = normalize(eye);

    float light_pow = dot(normal, light_dir);
    float specular = 0.0;
    float shininess = float(u_shininess);
    if (light_pow > 0.0) {
      specular = pow(dot(normal, normalize(light_dir + eye_dir)), shininess);
    }

    gl_FragColor = u_ambient;
    gl_FragColor.rgb += clamp(vec3(u_diffuse) * light_pow , 0.0, 1.0);
    gl_FragColor.rgb += clamp(vec3(u_specular) * specular , 0.0, 1.0);
    gl_FragColor.a = u_diffuse.w;
    if(vColor != vec3(0,0,0))
      gl_FragColor=vec4(vColor,1.);
  }`;

  vertexShader = loadShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  loadProgram(gl, vertexShader, fragmentShader);

  var object_count = input.objects.length;
  for(var i = 0; i < object_count; ++i){
    createObject(input.objects[i].position, input.objects[i].triangles, input.objects[i].normal, input.objects[i].material, input.objects[i].scale, input.objects[i].rotation, input.objects[i].translation);
  }

  aspect_ratio = 16.0/9.0;
  resize();
  if(typeof cameraInitInfo === 'undefined' || cameraInitInfo == ''){
    center = input.bound.center;
    camera = {};
    camera.rho = input.bound.radius / Math.tan(Math.PI/180.0*35.0/2.0);
    camera.theta = 45.0;
    camera.phi = 45.0;
  }
  else{
    // console.log(cameraInitInfo);
    var array = cameraInitInfo.split(" ").map(Number);
    center = array.slice(0,3);
    camera = {};
    camera.rho = array[3];
    camera.theta = array[4];
    camera.phi = array[5];
  }
  
  drawScene();
  
  canvas.addEventListener('wheel', wheelZoom, false);

  canvas.addEventListener('mousedown',function(event){
    last_x = event.clientX;
    last_y = event.clientY;
    canvas.addEventListener('mousemove', mouseMoveTransform, false);
  },false);

  window.addEventListener('mouseup',function(event){
    canvas.removeEventListener('mousemove', mouseMoveTransform, false);
    last_x = null;
    last_y = null;
  },false);

  window.addEventListener('resize', function(){
    resize();
    drawScene();
  }, false);

  canvas.oncontextmenu = function(){
    return false;
  }
}

function resize(){
  gl.canvas.width = gl.canvas.clientWidth;
  gl.canvas.height = gl.canvas.width/aspect_ratio;
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
}

function wheelZoom(event){
  var zoom_alpha = .001 * camera.rho

  var zoom_amount = camera.rho + zoom_alpha * event.deltaY;
  camera.rho = zoom_amount > 0.0? zoom_amount : 0.0;

  drawScene();
  event.preventDefault();
  return;
}

function mouseMoveTransform(event){
  delta_x = (last_x - event.clientX);
  delta_y = (last_y - event.clientY);

  if(event.buttons == 1){
    mouseMoveRotate(delta_x, delta_y);
  } if(event.buttons == 2){
    mouseMovePan(delta_x, delta_y);
  }

  last_x = event.clientX;
  last_y = event.clientY;
  event.preventDefault();
  return;
}

function mouseMoveRotate(delta_x, delta_y){
  rotate_alpha = 0.5;

  camera.phi = fmod(camera.phi - delta_x * rotate_alpha, 360.0);
  camera.theta = fmod(camera.theta - delta_y * rotate_alpha, 360.0);

  drawScene();
}

function mouseMovePan(delta_x, delta_y){
    var pan_alpha = .005 * camera.rho
    var up = getUp();
    var side = v3.normalize(v3.cross(up, getCamera()));

    center[0] += (side[0]*delta_x - up[0]*delta_y) * pan_alpha
    center[1] += (side[1]*delta_x - up[1]*delta_y) * pan_alpha
    center[2] += (side[2]*delta_x - up[2]*delta_y) * pan_alpha

    drawScene();
}


function fmod(n, d){
  while(n > d){
    n = n - d;
  }
  while(n < 0){
    n = n + d
  }
  return n;
}

function loadShader(gl, type, src){
  var shader = gl.createShader(type);
  gl.shaderSource(shader, src);
  gl.compileShader(shader);

  return shader;
}

function loadProgram(gl, vertexShader, fragmentShader){
  program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  gl.useProgram(program);

  v_position_attr = gl.getAttribLocation(program, 'a_v_position');
  gl.enableVertexAttribArray(v_position_attr);

  v_normal_attr = gl.getAttribLocation(program, 'a_v_normal');
  gl.enableVertexAttribArray(v_normal_attr);

}

function getCamera(){
  var x = camera.rho * Math.cos(camera.phi * Math.PI / 180.0) * Math.sin(camera.theta * Math.PI / 180.0) + center[0];
	var y = camera.rho * Math.cos(camera.theta * Math.PI / 180.0) + center[1];
	var z = camera.rho * Math.sin(camera.phi * Math.PI / 180.0) * Math.sin(camera.theta * Math.PI / 180.0) + center[2];
	
  return [x,y,z];
}

function getUp(){
  var up_x = Math.cos(camera.phi * Math.PI / 180.0) * Math.sin((90+camera.theta) * Math.PI / 180.0);
  var up_y = Math.cos((90+camera.theta) * Math.PI / 180.0);
  var up_z = Math.sin(camera.phi * Math.PI / 180.0) * Math.sin((90+camera.theta) * Math.PI / 180.0);
  return v3.normalize([up_x,up_y,up_z]);
}

function getSide(){
  var side_x = Math.cos((camera.phi-90) * Math.PI / 180.0) * Math.sin(camera.theta * Math.PI / 180.0);
  var side_y = Math.cos(camera.theta * Math.PI / 180.0);
  var side_z = Math.sin((camera.phi+90) * Math.PI / 180.0) * Math.sin(camera.theta * Math.PI / 180.0);
  return v3.normalize([side_x,side_y,side_z]);
}

function drawScene(){
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  var perspective = m4x4.perspective(Math.PI/180.0*35.0, aspect_ratio, 0.1, 100.0);
  u_P = gl.getUniformLocation(program, "u_P");
  gl.uniformMatrix4fv(u_P, false, new Float32Array(m4x4.flatten(perspective)));

  var camera = getCamera();
  var up = getUp();
  var view = m4x4.lookAt(camera, center, up);
  u_V = gl.getUniformLocation(program, "u_V");
  gl.uniformMatrix4fv(u_V, false, new Float32Array(m4x4.flatten(view)));
  //var view_inv = m4x4.transpose(m4x4.invert(view));
  // u_V_Inv = gl.getUniformLocation(program, "u_V_Inv");
  // gl.uniformMatrix4fv(u_V_Inv, false, new Float32Array(m4x4.flatten(view_inv)));

  var light =  v3.normalize([8, -20, 4]);
  u_light =  gl.getUniformLocation(program, "u_light");
  gl.uniform3fv(u_light, new Float32Array(camera)); //make camera the light source

  u_eye =  gl.getUniformLocation(program, "u_eye");
  gl.uniform3fv(u_eye, new Float32Array(camera));

  u_M = gl.getUniformLocation(program, "u_M");
  u_M_Inv = gl.getUniformLocation(program, "u_M_Inv");

 // Clear the canvas - gray background color 
  gl.clearColor(0.75, 0.75, 0.75, 0.9);
  gl.clear(gl.COLOR_BUFFER_BIT);  
  var objects = obj_vertices_buffer.length;
  for(var i = 0; i < objects; ++i){
    drawObject(i);
  }

  // draw axis
  if(debugMode)
  {
    v_color_attr = gl.getAttribLocation(program, 'a_v_color');
    gl.enableVertexAttribArray(v_color_attr);
    drawAxes();
  }

}

/* Matrices & Vector Helper Functions */

class m4x4{
  static zeros(){
    return [[0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]];
  }

  static eye(){
      return [[1, 0, 0, 0],
              [0, 1, 0, 0],
              [0, 0, 1, 0],
              [0, 0, 0, 1]];
  }

  static multiply(a, b){
    var res = m4x4.zeros();
    for(var i = 0; i != 4; ++i){
      for(var j = 0; j != 4; ++j){
        for(var k = 0; k!= 4; ++k){
          res[i][j] += a[i][k] * b[k][j];
        }
      }
    }
    return res;
  }


  static translate(v){
    return [[1.0,  0.0,  0.0,  0.0],
            [0.0,  1.0,  0.0,  0.0],
            [0.0,  0.0,  1.0,  0.0],
            [v[0], v[1], v[2], 1.0 ]];
  }

  static scale(v){
    return [[v[0], 0.0 , 0.0 , 0.0],
            [0.0 , v[1], 0.0 , 0.0],
            [0.0 , 0.0 , v[2], 0.0],
            [0.0 , 0.0 , 0.0 , 1.0]];
  }

  static rotate_x(d){
    var cos = Math.cos(d/180.0 * Math.PI);
    var sin = Math.sin(d/180.0 * Math.PI);
    return [[1.0, 0.0,  0.0, 0.0],
            [0.0, cos,  sin, 0.0],
            [0.0, -sin, cos, 0.0],
            [0.0, 0.0,  0.0, 1.0]];
  }

  static rotate_y(d){
    var cos = Math.cos(d/180.0 * Math.PI);
    var sin = Math.sin(d/180.0 * Math.PI);
    return [[cos, 0.0, -sin, 0.0],
            [0.0, 1.0, 0.0,  0.0],
            [sin, 0.0, cos,  0.0],
            [0.0, 0.0, 0.0,  1.0]];
  }

  static rotate_z(d){
    var cos = Math.cos(d/180.0 * Math.PI);
    var sin = Math.sin(d/180.0 * Math.PI);
    return  [[cos,  sin, 0.0, 0.0],
             [-sin, cos, 0.0, 0.0],
             [0.0,  0.0, 1.0, 0.0],
             [0.0,  0.0, 0.0, 1.0]];
  }

  static rotate(v){ 
  //this looks like ZYX order because everything is transposed
  // so actually it is XYZ order
    var m = m4x4.rotate_z(v[2]);
    m = m4x4.multiply(m4x4.rotate_y(v[1]), m);
    m = m4x4.multiply(m4x4.rotate_x(v[0]), m);
    return m;
  }
  // static rotate(v){ //this is in XYZ order!
  //   var m = m4x4.rotate_x(v[0]);
  //   m = m4x4.multiply(m4x4.rotate_y(v[1]), m);
  //   m = m4x4.multiply(m4x4.rotate_z(v[2]), m);
  //   return m;
  // }

  static rotate_quat(v){
    var q = v4.normalize(v);
    var m1 = [[q[0], q[3], -q[2], q[1]],
          [-q[3], q[0], q[1], q[2]],
          [q[2], -q[1], q[0], q[3]],
          [-q[1], -q[2], -q[3], q[0]]];

    var m2 = [[q[0], q[3], -q[2], -q[1]],
          [-q[3], q[0], q[1], -q[2]],
          [q[2], -q[1], q[0], -q[3]],
          [q[1], q[2], q[3], q[0]]];
    return m4x4.multiply(m2,m1);
  }

  static invert(a){
    var i = m4x4.zeros();
    i[0][0] =  a[1][1]*a[2][2]*a[3][3] - a[1][1]*a[2][3]*a[3][2] - a[2][1]*a[1][2]*a[3][3] + a[2][1]*a[1][3]*a[3][2] + a[3][1]*a[1][2]*a[2][3] - a[3][1]*a[1][3]*a[2][2];
    i[1][0] = -a[1][0]*a[2][2]*a[3][3] + a[1][0]*a[2][3]*a[3][2] + a[2][0]*a[1][2]*a[3][3] - a[2][0]*a[1][3]*a[3][2] - a[3][0]*a[1][2]*a[2][3] + a[3][0]*a[1][3]*a[2][2];
    i[2][0] =  a[1][0]*a[2][1]*a[3][3] - a[1][0]*a[2][3]*a[3][1] - a[2][0]*a[1][1]*a[3][3] + a[2][0]*a[1][3]*a[3][1] + a[3][0]*a[1][1]*a[2][3] - a[3][0]*a[1][3]*a[2][1];
    i[3][0] = -a[1][0]*a[2][1]*a[3][2] + a[1][0]*a[2][2]*a[3][1] + a[2][0]*a[1][1]*a[3][2] - a[2][0]*a[1][2]*a[3][1] - a[3][0]*a[1][1]*a[2][2] + a[3][0]*a[1][2]*a[2][1];

    i[0][1] = -a[0][1]*a[2][2]*a[3][3] + a[0][1]*a[2][3]*a[3][2] + a[2][1]*a[0][2]*a[3][3] - a[2][1]*a[0][3]*a[3][2] - a[3][1]*a[0][2]*a[2][3] + a[3][1]*a[0][3]*a[2][2];
    i[1][1] =  a[0][0]*a[2][2]*a[3][3] - a[0][0]*a[2][3]*a[3][2] - a[2][0]*a[0][2]*a[3][3] + a[2][0]*a[0][3]*a[3][2] + a[3][0]*a[0][2]*a[2][3] - a[3][0]*a[0][3]*a[2][2];
    i[2][1] = -a[0][0]*a[2][1]*a[3][3] + a[0][0]*a[2][3]*a[3][1] + a[2][0]*a[0][1]*a[3][3] - a[2][0]*a[0][3]*a[3][1] - a[3][0]*a[0][1]*a[2][3] + a[3][0]*a[0][3]*a[2][1];
    i[3][1] =  a[0][0]*a[2][1]*a[3][2] - a[0][0]*a[2][2]*a[3][1] - a[2][0]*a[0][1]*a[3][2] + a[2][0]*a[0][2]*a[3][1] + a[3][0]*a[0][1]*a[2][2] - a[3][0]*a[0][2]*a[2][1];

    i[0][2] =  a[0][1]*a[1][2]*a[3][3] - a[0][1]*a[1][3]*a[3][2] - a[1][1]*a[0][2]*a[3][3] + a[1][1]*a[0][3]*a[3][2] + a[3][1]*a[0][2]*a[1][3] - a[3][1]*a[0][3]*a[1][2];
    i[1][2] = -a[0][0]*a[1][2]*a[3][3] + a[0][0]*a[1][3]*a[3][2] + a[1][0]*a[0][2]*a[3][3] - a[1][0]*a[0][3]*a[3][2] - a[3][0]*a[0][2]*a[1][3] + a[3][0]*a[0][3]*a[1][2];
    i[2][2] =  a[0][0]*a[1][1]*a[3][3] - a[0][0]*a[1][3]*a[3][1] - a[1][0]*a[0][1]*a[3][3] + a[1][0]*a[0][3]*a[3][1] + a[3][0]*a[0][1]*a[1][3] - a[3][0]*a[0][3]*a[1][1];
    i[3][2] = -a[0][0]*a[1][1]*a[3][2] + a[0][0]*a[1][2]*a[3][1] + a[1][0]*a[0][1]*a[3][2] - a[1][0]*a[0][2]*a[3][1] - a[3][0]*a[0][1]*a[1][2] + a[3][0]*a[0][2]*a[1][1];

    i[0][3] = -a[0][1]*a[1][2]*a[2][3] + a[0][1]*a[1][3]*a[2][2] + a[1][1]*a[0][2]*a[2][3] - a[1][1]*a[0][3]*a[2][2] - a[2][1]*a[0][2]*a[1][3] + a[2][1]*a[0][3]*a[1][2];
    i[1][3] =  a[0][0]*a[1][2]*a[2][3] - a[0][0]*a[1][3]*a[2][2] - a[1][0]*a[0][2]*a[2][3] + a[1][0]*a[0][3]*a[2][2] + a[2][0]*a[0][2]*a[1][3] - a[2][0]*a[0][3]*a[1][2];
    i[2][3] = -a[0][0]*a[1][1]*a[2][3] + a[0][0]*a[1][3]*a[2][1] + a[1][0]*a[0][1]*a[2][3] - a[1][0]*a[0][3]*a[2][1] - a[2][0]*a[0][1]*a[1][3] + a[2][0]*a[0][3]*a[1][1];
    i[3][3] =  a[0][0]*a[1][1]*a[2][2] - a[0][0]*a[1][2]*a[2][1] - a[1][0]*a[0][1]*a[2][2] + a[1][0]*a[0][2]*a[2][1] + a[2][0]*a[0][1]*a[1][2] - a[2][0]*a[0][2]*a[1][1];

    var det = a[0][0]*i[0][0] + a[0][1]*i[1][0] + a[0][2]*i[2][0] + a[0][3]*i[3][0];
    if(det == 0){
      return -1;
    }

    for(var j = 0; j != 4; ++j){
      for(var k = 0; k != 4; ++k){
        i[j][k] = i[j][k] / det;
      }
    }

    return i;
  }

  static transpose(m){
    return[[m[0][0], m[1][0], m[2][0], m[3][0]],
           [m[0][1], m[1][1], m[2][1], m[3][1]],
           [m[0][2], m[1][2], m[2][2], m[3][2]],
           [m[0][3], m[1][3], m[2][3], m[3][3]]];
  }
  // transpose a 3*3 matrix
  static transposeMat3(m){
    return[[m[0][0], m[1][0], m[2][0]],
           [m[0][1], m[1][1], m[2][1]],
           [m[0][2], m[1][2], m[2][2]]];
  }

  // inverse a 3*3 matrix
  static invertMat3(m){
    var det = m[0][0] * (m[1][1]  * m[2][2] - m[2][1] * m[1][2]) 
    - m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) 
    + m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
    var invdet = 1.0 / det;
    var minv = [];
    minv[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet;
    minv[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet;
    minv[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet;
    minv[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet;
    minv[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet;
    minv[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet;
    minv[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet;
    minv[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet;
    minv[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet;
    return minv;

  }

  static flatten(a){
    return [].concat.apply([], a);
  }

  static lookAt(eye, center, up){
    var f = v3.normalize(v3.subtract(eye, center));//eye - center = backward
    var s = v3.cross(up, f);//up x backward = right
    var u = v3.normalize(v3.cross(f, s));//back x right = up
    s = v3.normalize(s);
    /*var m = [[ s[0], s[1], s[2], 0 ],
             [ u[0], u[1], u[2], 0 ],
             [ f[0], f[1], f[2], 0 ],
             [   0 ,   0 ,   0,  1 ]];
    var t = [[ 1, 0, 0, 0 ],
             [ 0, 1, 0, 0 ],
             [ 0, 0, 1, 0 ],
             [ -eye[0], -eye[1], -eye[2], 1 ]];*/
    //return m4x4.multiply(m, t);
    return  [[ s[0], u[0], f[0], 0 ],
             [ s[1], u[1], f[1], 0 ],
             [ s[2], u[2], f[2], 0 ],
             [ -v3.dot(s, eye) , -v3.dot(u, eye), -v3.dot(f, eye),  1 ]];	 
			 
  }

  static lookAt_quat(eye, center, up){ // return the rotation quaternion from view matrix
    var f = v3.normalize(v3.subtract(eye, center));//backward
    var s = v3.cross(up, f);//right
    var u = v3.normalize(v3.cross(f, s));//up
    s = v3.normalize(s);
	//the following computation is based on 3x3 mat [right, up, forward]
    var w = Math.sqrt(1.0 + s[0] + u[1] + f[2])/2.0;
    var x = (u[2] - f[1])/(4.0 * w);
    var y = (f[0] - s[2]) / (4.0 * w);
    var z = (s[1] - u[0]) / (4.0 * w);
    return  [x,y,z,w];
  }

  static perspective (fov, aspect, near, far){
    var f =  1.0 / Math.tan(fov/2.0);
    var range = 1.0 / (near - far);
    return [[f/aspect, 0, 0,                      0],
            [0,        f, 0,                      0],
            [0,        0, (near + far) * range,  -1],
            [0,        0, near * far * range * 2, 0]];
  }
}

class v3{
  static zeros(){
    return [0, 0, 0];
  }
  static magnitude(a){
    var sum = 0;
    for(var i = 0; i != 3; ++i){
      sum += (a[i]*a[i]);
    }
    return Math.sqrt(sum);
  }

  static normalize(a){
    var res = v3.zeros();
    var sum = v3.magnitude(a);
    for(var i = 0; i != 3; ++i){
      res[i] = a[i]/sum;
    }
    return res;
  }

  static cross(a, b){
    return [ a[1]*b[2] - a[2]*b[1],
             a[2]*b[0] - a[0]*b[2],
             a[0]*b[1] - a[1]*b[0]];
  }

  static dot(a, b){
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  }

  static subtract(a, b){
    var res = v3.zeros();
    for(var i = 0; i != 3; ++i){
      res[i] = a[i] - b[i];
    }
    return res;
  }
}

class v4{
  static zeros(){
    return [0, 0, 0, 0];
  }

  static magnitude(a){
    var sum = 0;
    for(var i = 0; i != 4; ++i){
      sum += (a[i]*a[i]);
    }
    return Math.sqrt(sum);
  }

  static normalize(a){
    var res = v4.zeros();
    var sum = v4.magnitude(a);
    for(var i = 0; i != 4; ++i){
      res[i] = a[i]/sum;
    }
    return res;
  }
}

function CameraPos(){ //Sends the position value corresponding to position parameter in the .scn file
	//var camera1= getCamera();
	//var centery=center[1]/10;
	//var strings = center[0].toString() + " " + centery.toString() + " " + center[2].toString();
	var campos = getCamera();
	var strings = campos[0].toString() + " " + campos[1].toString() + " " + campos[2].toString();	
	return strings;
}
//Sends the orientation value corresponding to orientation parameter in the .scn file
//(rho, theta, phi) -> quaternion(x,y,z,k)
function CameraOrientation(){ 
	// var rho = ((camera.rho - 30)/2) * Math.PI / 180.0;
	// var theta = ((camera.theta + 180)/2) * Math.PI / 180.0;
	// var phi = ((camera.phi + 180 ) /2) * Math.PI / 180.0;
	// var x = Math.cos(rho) * Math.cos(theta) * Math.cos(phi) + Math.sin(rho) * Math.sin(theta) * Math.sin(phi);
	// var y = Math.sin(rho) * Math.cos(theta) * Math.cos(phi) - Math.cos(rho) * Math.sin(theta) * Math.sin(phi);
	// var z = Math.cos(rho) * Math.sin(theta) * Math.cos(phi) + Math.sin(rho) * Math.cos(theta) * Math.sin(phi);
	// var k = Math.cos(rho) * Math.cos(theta) * Math.sin(phi) - Math.sin(rho) * Math.sin(theta) * Math.cos(phi);
	// var strings = x.toString() + " " + y.toString() + " " + z.toString() + " " + k.toString();
  var camera = getCamera();
  var up = getUp();
  var rot_quat = m4x4.lookAt_quat(camera, center, up);
	var strings = rot_quat.toString();
	return strings;
}

//Read the center and camera position (rho,theta,phi) for next init() in refreshing the page
function CameraInitInfo(){
  if(camera != ' '){
    var strings = center[0].toString()+" "+center[1].toString()+" "+center[2].toString()+" "
    +camera.rho.toString()+" "+camera.theta.toString()+" "+camera.phi.toString();
    return strings;
  }
  
}