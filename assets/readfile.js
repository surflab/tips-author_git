function stringsToFloats(input){
  var output = [];
  for(var i = 0; i < input.length; ++i){
    output.push(parseFloat(input[i]));
  }
  return output;
}

function stringsToInts(input){
  var output = [];
  for(var i = 0; i < input.length; ++i){
    output.push(parseInt(input[i]));
  }
  return output;
}

function quadsToTriangles(input){
  var output = [];
  for(var i = 0; i < input.length; i+=4){
    output = output.concat(input.slice(i,i+3));
    output = output.concat(input.slice(i+2,i+4));
    output.push(input[i]);
  };
  return output;
}

function hexahedraToQuads(input){
  var output;
  for(var i = 0; i < input.length; i+=8){
    output.concat([input[i+0], input[i+1], input[i+2], input[i+3]]);
    output.concat([input[i+4], input[i+7], input[i+6], input[i+5]]);
    output.concat([input[i+1], input[i+0], input[i+4], input[i+5]]);
    output.concat([input[i+1], input[i+5], input[i+6], input[i+2]]);
    output.concat([input[i+2], input[i+6], input[i+7], input[i+3]]);
    output.concat([input[i+0], input[i+3], input[i+7], input[i+4]]);
  }
  return output;
}

function calculateCenter(input){
  var max_x = input[0];
  var min_x = input[0];
  var max_y = input[1];
  var min_y = input[1];
  var max_z = input[2];
  var min_z = input[2];
  for(var i = 2; i < input.length; i+=3){
    if(max_x < input[i]) max_x = input[i];
    else if(min_x > input[i]) min_x = input[i];
    if(max_y < input[i + 1]) max_y = input[i + 1];
    else if(min_y > input[i + 1]) min_y = input[i + 1];
    if(max_z < input[i + 2]) max_z = input[i + 2];
    else if(min_z > input[i + 2]) min_z = input[i + 2];
  }
  var x = (max_x - min_x)/2 + min_x;
  var y = (max_y - min_y)/2 + min_y;
  var z = (max_z - min_z)/2 + min_z;
  return [x, y, z];
}

function parseMaterial(input){
  input = input.split(/\s+/);
  var output = {};
  var index = input.indexOf('Diffuse');
  output.diffuse = (index >= 0)? stringsToFloats(input.slice(index + 2,index + 6)) : [0.5, 0.5, 0.5 , 1.0];
  index = input.indexOf('Ambient');
  output.ambient = (index > 0)? stringsToFloats(input.slice(index + 2,index + 6)) : [0.9, 0.9, 0.9 , 1.0];
  index = input.indexOf('Specular');
  output.specular = (index >= 0)? stringsToFloats(input.slice(index + 2,index + 6)) : [0,0,0, 1.0];
  index = input.indexOf('Emissive');
  output.emissive = (index >= 0)? stringsToFloats(input.slice(index + 2,index + 6)) : [0,0,0, 1.0];
  index = input.indexOf('Shininess');
  output.shininess = (index >= 0)? parseInt(input[index + 2]) : 0;

  return output;
}


function parsePreview(input){
  var output = {};
  output.position = stringsToFloats(input.getAttribute('vertices').trim().split(/\s+/));
  output.normal = stringsToFloats(input.getAttribute('normals').trim().split(/\s+/));
  output.material = parseMaterial(input.getAttribute('material'));
  output.triangles = stringsToInts(input.getAttribute('indices').trim().split(/\s+/));
  output.rotation = stringsToFloats(input.getAttribute('rotate').trim().split(/\s+/));
  output.translation = stringsToFloats(input.getAttribute('translate').trim().split(/\s+/));
  output.scale = stringsToFloats(input.getAttribute('scale').trim().split(/\s+/));
  output.center = stringsToFloats(input.getAttribute('center').trim().split(/\s+/));
  output.radius = parseFloat(input.getAttribute('radius').trim());
  return output;
}
function parseBound(input){
  var output = {};
  output.center = stringsToFloats(input.getAttribute('center').trim().split(/\s+/));
  //console.log(output.center);
  output.radius = parseFloat(input.getAttribute('radius').trim());
  return output;
}


function readFile(input){
  var output = {}
  // get objects
  console.log("Input : " + input)
  var nodes = input.getElementsByTagName('PreviewModel');
  var node_count = nodes.length;
  output.objects = [];
  for(var i = 0; i < node_count; ++i){
    var preview = parsePreview(nodes[i]);
    output.objects.push(preview);
  }
  
  // get bounds
  nodes = input.getElementsByTagName('PreviewBound');
  if(nodes.length > 0){
    output.bound = parseBound(nodes[0]);
  }

  //console.log(output);
  return output;
}
var source;
function loadDoc(src, cameraInitInfo) {
  source=src;
  console.log(src);
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var res = readFile(this.responseXML);
      init(res,cameraInitInfo);
    }
  };

xhttp.open("GET", src, true);
xhttp.send();
}

