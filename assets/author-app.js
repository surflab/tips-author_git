var authorApp = angular.module('author', ['ngRoute', 'ui.bootstrap' ]).config(['$compileProvider',function($compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|tips):/);
}]);

authorApp.directive('previewObj', [function(){
  return {
    restrict: 'E',
    scope: {
      source: '@src',
      model:'=',
      init:'@init'
    },
    link: function(scope, element, attrs){
      loadDoc(scope.source,scope.init);

    },
    template: '<canvas id="c"></canvas>'
  }
}]);

authorApp.directive('autoSaveForm', [ '$timeout', function($timeout) {
  return {
    require: ['^form'],
    link: function($scope, $element, $attrs, $ctrls) {
      var $formCtrl = $ctrls[0];
      var savePromise = null;
      var expression = $attrs.autoSaveForm || 'true';

      $scope.$watch(function() {
        if($formCtrl.$valid && $formCtrl.$dirty) {
          if(savePromise)
            $timeout.cancel(savePromise);

          savePromise = $timeout(function() {
            savePromise = null;
            // Still valid?
            if($formCtrl.$valid) {
              $formCtrl.saving = true;
              if($scope.$eval(expression) !== false)
                $formCtrl.$setPristine();

              $formCtrl.saving = false;
            }
          }, 3000);
        }
      });
    }
  }
}]);

authorApp.directive('inputDataFile', [ function() {
  return {
    restrict: 'E',
    scope: { data: '=data', downloadUrl: '=downloadUrl', uploadUrl: '=uploadUrl', refreshUploadUrl: '&', isDisabled: '=isDisabled' },
    //scope: true,  // inherit scope from parent (vs isolated scope which is default) so that refreshUploadUrl is visible
    templateUrl: 'assets/partials/data-file.html',
    controller: ['$scope', '$http', function($scope, $http){
      $scope.upload = function() {
      };
    }]
  };
}]);

authorApp.directive('bootstrapFileinput', function($timeout){
  return {
    restirct: 'A',
    // scope: true by default since this is a directive restricted to 'A' (attribute matching)
    link: function(scope, element, attrs) {
      jQuery(element).fileinput({
          showPreview: false,
          showCaption: true,
          showUpload: false,
          uploadLabel: "Apply",
          removeLabel: "Cancel",
          allowedFileExtensions: ['xml', 'zip', 'scn'],
          elErrorContainer: "#errorBlock",
      })
      .on('change', function(event) {
        scope.refreshUploadUrl();
      })
      .on('fileuploaded', function(event, data, previewId, index){
        jQuery(element).fileinput('refresh', { showUpload: false, showRemove: false, browseLabel: "Done" });
      });

      scope.$watch(attrs.fileData, function(fd){
        if (!fd)
          jQuery(element).fileinput('refresh', { initialCaption: "No data file", browseLabel: "Upload"});
        else
          jQuery(element).fileinput('refresh', { initialCaption: fd.filename + " (" + fd.size + " bytes)", browseLabel: "Replace"});
      });
      scope.$watch(attrs.uploadUrl, function(url){
        jQuery(element).fileinput('refresh', { uploadUrl: url, showUpload: typeof(url) == 'string' && url > '' });
      });
    }
  }
});

authorApp.directive('typeaheadOpenOnFocus', function ($timeout) { // http://stackoverflow.com/a/39852600
       return {                                                   // Show dropdown menu on click/focus
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            element.bind('click', function () {
                var vv = ctrl.$viewValue;
                ctrl.$setViewValue(vv ? vv+' ': ' ' );
                $timeout(function(){ctrl.$setViewValue(vv ? vv : '');},10)
            });
        }
    };
})

authorApp.controller('ToolOrganController', ['$scope', '$rootScope', '$http', '$route', '$routeParams', 'Model', function($scope, $rootScope, $http, $route, $routeParams, Model){
  var model = Model.toLowerCase();
  var newModel, apiURL, modelID;

  function setID(id){
    newModel = id == 'new';
    apiURL = 'api/' + model + 's/' + (newModel ? '666' : id);
    $http.get('api/data/' + model + '/' + id + '/upload').success(function(data){
      modelID = id;
      $scope.uploadUrl = data; // data: Blob key/URL(When you upload a file to Blobstore, Blobstore creates a blob based on your file and adds a corresponding key, called a blob key)
    });
    // $route.updateParams({id: data.id});
  }
  setID($routeParams.id);

  function csvVerbs(newList){
    if(arguments.length){
      this.verbs = newList.split(',').map(function(a){return a.trim();})
    } else {
      return (this.verbs || []).join(',');
    }
  }

  if(!newModel){
    $http.get(apiURL).success(function(data){
      data.csvVerbs = csvVerbs;
      $scope[model] = data;// data: contains the organ/tool data. corresponds to $scope.organ or $scope.tool. In the .html file it is accessed using organ/tool identifier. e.g. data.name here corresponds to organ.name

   }).error(function(data){
      window.alert('Error: ' + data);
    });
  }else{
    $scope[model] = { csvVerbs: csvVerbs }
  }

  function setEditable(user){
    if (user != null)
      $scope.isEditable = user.isAdmin || user.isArtist;
  }
  setEditable($rootScope.activeUser);

  $http.get('api/autocomplete').success(function(data){
     $scope.organs = data["organ"];  // data: organ: Array[], tool: Array[], verb : Array[]
  });

  $scope.refreshUploadUrl = function refreshUploadUrl(){  // called from bootstrapFileinput to regenerate upload url after fileuploaderror generated from the backend (python)
    $http.get('api/data/' + model + '/' + modelID + '/upload').success(function(data){
      $scope.uploadUrl = data; // data: Blob key/URL(When you upload a file to Blobstore, Blobstore creates a blob based on your file and adds a corresponding key, called a blob key)
    });
  }

  $scope.addRelatedOrgan = function addRelatedOrgan(o,related){
     var r = $scope.organs.find(function (o){ return o.name == related; });
     o.related_organs = o.related_organs || [];
     if(typeof(r) != 'undefined')
       o.related_organs.push(r);
  }
  $scope.addConnectedRelatedOrgan = function addConnectedRelatedOrgan(o,related){
    var r = $scope.organs.find(function (o){ return o.name == related; });
    o.connected_related_organs = o.connected_related_organs || [];
    if(typeof(r) != 'undefined')
      o.connected_related_organs.push(r);
  }
  $scope.addVariantOrgan = function addVariantOrgan(o,related){
    var r = $scope.organs.find(function (o){ return o.name == related; });
    o.variant_organs = o.variant_organs || [];
    if(typeof(r) != 'undefined')
      o.variant_organs.push(r);
  }

  $scope.removeRelated = function removeRelated(o,id){
     o.related_organs = o.related_organs || [];
     o.related_organs = o.related_organs.filter(function(r){ return r.id != id; });
  }
  $scope.removeConnectedRelated = function removeConnectedRelated(o,id){
    o.connected_related_organs = o.connected_related_organs || [];
    o.connected_related_organs = o.connected_related_organs.filter(function(r){ return r.id != id; });
  }
  $scope.removeVariant = function removeVariant(o,id){
    o.variant_organs = o.variant_organs || [];
    o.variant_organs = o.variant_organs.filter(function(r){ return r.id != id; });
  }
  
  $scope.save = function save() {
    $http({ url: apiURL, data: this[model], method: 'PUT' }).success(function(data){
      if(newModel) setID(data.id);
    });
  }
}]);

function removeById(l,id){
  for(var i = 0;i < l.length; i++)
    if(l[i].id == id){
      l.splice(i, 1);
      return
    }
}

authorApp.factory('confirmDelete', ['$uibModal', function($uibModal){
  return function(item){
    return $uibModal.open({
      templateUrl: 'assets/partials/delete-confirmation.html',
      controller:['$scope', '$uibModalInstance', function($scope,$uibModalInstance){
        $scope.item = item;
        $scope.ok = function(){ $uibModalInstance.close(true); }
        $scope.cancel = function(){ $uibModalInstance.dismiss(false); }
      }],
      size: '',
    });
  }
}]);

authorApp.controller('ProcedureController', ['$scope', '$rootScope', '$http', '$route', '$routeParams', '$uibModal', '$sce', '$location', '$window', function($scope, $rootScope, $http, $route, $routeParams, $uibModal, $sce, $location, $window){
  $scope.proc = { name: '', stages: [], quiz: {}, cameraSetup:{} };
  $scope.saveCamera = false;
  $scope.setSaveCameraFalse = function() {
    $scope.saveCamera = false;
  }
  $scope.setSaveCameraTrue = function() {
    $scope.saveCamera = true;
  }
  $scope.camerapos = function() { // this function sends the camera position values to python
	  return CameraPos(); //it is defined in fx.js
  };
  $scope.cameraorientation = function() { // this function sends the camera  orientation values to python
	  return CameraOrientation(); //it is defined in fx.js
  };
  
  // by default don't show the dropdown to select existing headerfiles
  $scope.selectExisting = false;

  $scope.headerfileList = {"filename": String, "keyname": String};
  $http.get('api/existingHeaderfiles').success(function(data){
      $scope.headerfileList = data;
  });

  $scope.autocompleteList = {"organ": [], "tool": [], "verb": [], "safety_verb":[]};
  $http.get('api/autocomplete').success(function(data){
    $scope.autocompleteList = data['verbs'];
    $scope.safetyOrganList = data['organ']
  });

  $scope.quizSelectButton = 'Select quiz associated with the procedure';
  $scope.quizzes = [];
  $http.get('api/quizzes/').success(function(data) {
      $scope.quizzes = data.sort(function(a, b) { return a.name.localeCompare(b.name) });
      $scope.quizzes.push({id: -9999, name: 'Create new quiz', url: ''});
      //console.log('quizzes', $scope.quizzes);
  });
  function fixUpProc(proc){
    //console.log('proc in fixUpProc', proc);
    proc.stages = proc.stages || [];
    for(var i = 0; i < proc.stages.length; i++){
      proc.stages[i].id = i;
      proc.stages[i].tasks = proc.stages[i].tasks || [];
      for(var j = 0; j < proc.stages[i].tasks.length; j++)
        proc.stages[i].tasks[j].id = j;
    }
    return proc;
  }

  var newModel = $routeParams.id == 'new';
  var id = Number(newModel ? 666 : $routeParams.id)
  var apiURL = 'api/procedures/' + id;
  $scope.editMode = newModel || ($routeParams.mode == 'edit');
  if(!newModel){
    $http.get(apiURL).success(function(data){
      $scope.proc = fixUpProc(data);
      $scope.quizSelectButton = $scope.proc.quiz.name;
    }).error(function(data){
      window.alert('Error: ' + data);
    });
  }

  var reloadFlag = false;
  $scope.organChanged = function() {
    reloadFlag = true;
  }

  function setEditable(user){
    if (user != null)
		// $scope.isEditable = user.isAdmin;
       $scope.isEditable = user.isAdmin || user.isAuthor;
  }
  setEditable($rootScope.activeUser);

  var modelID;
  function setID(id){
    $http.get('api/data/procedure/' + id + '/upload').success(function(data){
      modelID = id;
      $scope.uploadUrl = data; // data: Blob key/URL(When you upload a file to Blobstore, Blobstore creates a blob based on your file and adds a corresponding key, called a blob key)
    });
  }
  setID($routeParams.id);


  $scope.refreshUploadUrl = function refreshUploadUrl(){  // called from bootstrapFileinput to regenerate upload url after fileuploaderror generated from the backend (python)
    $http.get('api/data/procedure/' + modelID + '/upload').success(function(data){
      $scope.uploadUrl = data; // data: Blob key/URL(When you upload a file to Blobstore, Blobstore creates a blob based on your file and adds a corresponding key, called a blob key)
    });
  }

  $scope.getGenStatus = function(){
    $http.get('api/genstatus/' + id).success(function(data){
      $scope.genstatus = data;
    });
  }

  $scope.getSceneForTIPS = function() {
    $http.get('api/genstatus/' + id).success(function(data) {
      $scope.genstatus = data;
      if($scope.genstatus.generatable==1) {
        let protocol = $location.protocol();
        let host = $location.host();
        let port = $location.port();
        let baseUrl = host;
        if(port != undefined || port != 'undefined' || port != '') {
          baseUrl = baseUrl + ':' + port;
        }
        let url = baseUrl + '/api/genscene/' + $scope.proc.id + '/' + $scope.proc.cameraSetup.position + '/' + $scope.proc.cameraSetup.quaternion + '/' + $scope.proc.cameraSetup.initInfo;
        //console.log('url', url);
        //$scope.urlForTIPS = 'tips:load ' + protocol + '://' + encodeURI(url); //used
		$scope.urlForTIPS = 'tips:load' + protocol + '://' + encodeURI(url); //currently deployed, fixed the T-Trainee page openning SOFA issue
        //$scope.urlForTIPS = 'tips:load ' + protocol + '://' + encodeURI(url); 
        //$scope.urlForTIPS = encodeURI('tips:load ' + protocol + '://' + url); 
      } else {
        $window.alert('Could not ready scene for TIPS! Please try again.');
      }
    });
  }

  $scope.doneTraining = function() {
    $window.close();
  }

  $scope.save = function() {
    $http({ url: apiURL, data: this.proc, method: 'PUT' }).success(function(data){
      if(newModel){
        setID(data.id);
        id = data.id;
        apiURL = 'api/procedures/' + id;
        newModel = false;
        $scope.proc.id = id;//this access the procedure objs
      }

      $scope.$watch($scope.proc.stages.tasks, function() {
        if(reloadFlag) {
          loadDoc("/api/genpreview/" + id, $scope.proc.cameraSetup.initInfo);
          reloadFlag = false;
        }
      });

    });
  }

  $scope.selectQuiz = function(quiz) {
    console.log('selected quiz', quiz);
    if(quiz.id == -9999) {
      $scope.quizSelectButton = "Please send the set of your questions to jorg.peters@gmail.com"
    } else {
      $scope.quizSelectButton = quiz.name;
      $scope.proc.quiz = quiz;
      $scope.save();
    }
  }

  $scope.toggleHeaderSelection = () => {
    $scope.selectExisting = !$scope.selectExisting;
  }

  $scope.selectHeader = (selectedHeader) => {
    $http({ url: `/api/selectExistingHeaderfile/${$scope.proc.id}/${selectedHeader.blobkey}`, method: 'PUT'}).success(() => {
      // refresh page data
      var apiURL = 'api/procedures/' + $scope.proc.id;
      $http.get(apiURL).success((data) => {
        $scope.proc = fixUpProc(data);
        $scope.selectExisting = !$scope.selectExisting;
      });
    });
  }

  $scope.getAssociatedOrgans = (task) => {
    $http({url: `/api/getOrgVariations/${task.organ}`, method: 'GET'}).success((data) => {
      task.orgVariations = data;
      $scope.save();
    });
  }

  $scope.getAssociatedTools = (task) => {
    $http({url: `/api/getToolVariations/${task.tool}`, method: 'GET'}).success((data) => {
      task.toolVariations = data;
      $scope.save();
    });
  }

  $scope.selectRandomOrgVariation = (task) => {
    task.orgVariation = task.orgVariations.organs[Math.floor(Math.random() * task.orgVariations.organs.length)];
  }

  $scope.selectRandomToolVariation = (task) => {
    task.toolVariation = task.toolVariations.tools[Math.floor(Math.random() * task.toolVariations.tools.length)];
  }

  $scope.removeStage = function(stage){
    var index = $scope.proc.stages.indexOf(stage);
    $scope.proc.stages.splice(index, 1);
    for(var i = index; i < $scope.proc.stages.length; i++)
      $scope.proc.stages[i].id = i;
  };
  $scope.removeTask = function(stage,task){
    var index = stage.tasks.indexOf(task);
    stage.tasks.splice(index, 1);
    for(var i = index; i < stage.tasks.length; i++)
      stage.tasks[i].id = i;
    reloadFlag = true;
  };
  $scope.insertTask = function(stage, id){
    var tmp = { id: id };
    for(var i = id; i < stage.tasks.length; i++){
      var x = stage.tasks[i];
      stage.tasks[i] = tmp;
      tmp = x;
    }
    stage.tasks.push(tmp);
    this.form.$setDirty();
  };

  $scope.swapTask = function(stage, id1, id2){
    if(id2 >= 0 && id2 < stage.tasks.length) {
    var x = stage.tasks[id1];
    stage.tasks[id1] = stage.tasks[id2];
    stage.tasks[id2] = x;
    stage.tasks[id1].id = id1;
    stage.tasks[id2].id = id2;
    }
  }
  
  $scope.addVideo = function(stage){
    // Open a modal that allows user to paste a youtube URL
    // then get a preview of that youtube URL and if it is
    // correct then proceed to save it
    $uibModal.open({
      templateUrl: 'assets/partials/video-dialog.html',
      controller: ['$scope','$uibModalInstance', function($scope,$uibModalInstance){
        var youtubeURL = "";
        var youtubeURLStartTime = "";
        var youtubeURLEndTime = "";
        $scope.close = function(){ $uibModalInstance.dismiss(false); };
        $scope.add = function(){
          //var u = $scope.youtubeID;
          var youtubeVideo = {};
          youtubeVideo.id = $scope.youtubeID;
          youtubeVideo.startTime = $scope.youtubeVideoStartTime;
          youtubeVideo.endTime = $scope.youtubeVideoEndTime;
          if(youtubeVideo) {
            stage.youtubeVideos = stage.youtubeVideos || [];
            stage.youtubeVideos.push(youtubeVideo);
            $uibModalInstance.close(true);
            console.log("YTD " + stage.youtubeVideos);
          }
        }
        $scope.youtubeURL = function(v){
          if(typeof(v) == 'undefined')
            return  youtubeURL;
          else {
            youtubeURL = v;
            var re = /(?:youtube\.com\/watch\?v=|youtu.be\/)([a-zA-Z0-9\-_]{11})/
            var r = re.exec(v);
            console.log(r);
            if(r!=null){
              $scope.youtubeID = r[1];
              $('iframe#youtubeFrame').attr('src','https://youtube.com/embed/'+$scope.youtubeID);
            }
          }
        }
        $scope.youtubeURLStartTime = function(v) {
          if(typeof(v) == 'undefined') {
            return youtubeURLStartTime;
          } else {
            youtubeURLStartTime = v;
            let startTime = v.split(':');
            $scope.youtubeVideoStartTime = parseInt(startTime[0]) * 60;
            $scope.youtubeVideoStartTime += parseInt(startTime[1]);
            $('iframe#youtubeFrame').attr('src','https://youtube.com/embed/' + $scope.youtubeID + '?start=' + $scope.youtubeVideoStartTime);
          }
        }
        $scope.youtubeURLEndTime = function(v) {
          if(typeof(v) == 'undefined') {
            return youtubeURLEndTime;
          } else {
            youtubeURLEndTime = v;
            let endTime = v.split(':');
            $scope.youtubeVideoEndTime = parseInt(endTime[0]) * 60;
            $scope.youtubeVideoEndTime += parseInt(endTime[1]);
            $('iframe#youtubeFrame').attr('src','https://youtube.com/embed/' + $scope.youtubeID + '?start=' + $scope.youtubeVideoStartTime + '&end=' + $scope.youtubeVideoEndTime);
          }
        }
      }]
    }).result.then(function(r){
      if(r) $scope.save();
    }, function() {

    });
  }
 
  $scope.editVideo = function(stage, video){
    // Open a modal that allows user to paste a youtube URL
    // then get a preview of that youtube URL and if it is
    // correct then proceed to save it
    console.log(video);
    
    $uibModal.open({
      templateUrl: 'assets/partials/video-dialog.html',
      controller: ['$scope','$uibModalInstance', function($scope,$uibModalInstance){
        var youtubeURL = "";
        var youtubeURLStartTime = "";
        var youtubeURLEndTime = "";
        var min = Math.floor(video.startTime/60);
        var sec = video.startTime%60;
        if(sec < 10) 
          sec = "0" + sec;
        var end_min = Math.floor(video.endTime/60);
        var end_sec = video.endTime%60;
        if(end_sec < 10) 
          end_sec = "0" + end_sec;
        $scope.videoEditFlag = true; 
        $scope.youtubeURL = "https://youtube.com/embed/" + video.id;
        $scope.youtubeURLStartTime = min + ":" + sec;
        $scope.youtubeURLEndTime = end_min + ":" + end_sec;
        $scope.close = function(){ $uibModalInstance.dismiss(false); };
        $scope.edit = function(){
          console.log("add function : " + stage.youtubeVideos)
          if(stage.youtubeVideos.length > 0) {
            for(let v in stage.youtubeVideos) { 
              if(video.id == stage.youtubeVideos[v].id) {
                let startTime = parseInt($scope.youtubeURLStartTime.split(':')[0]) * 60;
                startTime += parseInt($scope.youtubeURLStartTime.split(':')[1]);
                let endTime = parseInt($scope.youtubeURLEndTime.split(':')[0]) * 60;
                endTime += parseInt($scope.youtubeURLEndTime.split(':')[1]);
                console.log("Start : " + startTime);
                console.log("End : " + endTime);
                stage.youtubeVideos[v].startTime = startTime;
                stage.youtubeVideos[v].endTime = endTime;
              }
            }
            $scope.videoEditFlag = false;
            $uibModalInstance.close(true);
          }
        }
      }]
    }).result.then(function(r){
      if(r) $scope.save();
    });
  }
  
  $scope.deleteVideo = function(stage, video){
    console.log("Video : " + video);
    if(stage.youtubeVideos.length > 0) {
      for(let v in stage.youtubeVideos) {
        if(video.id == stage.youtubeVideos[v].id)
          stage.youtubeVideos.splice(v,1);
      }
    }
    $scope.save();
  }

  $scope.enable_safety = function(stage, task){
	  document.getElementById('task'+stage.id+'-'+task.id+'safety').removeAttribute("disabled");
  }
  $scope.disable_safety = function(stageId, taskId){
	  document.getElementById('task'+stage.id+'-'+task.id+'safety').setAttribute("disabled","disabled");
  }
  $scope.setCameraByUser = function(proc){
    var newCamera = { position: 0, quaternion: 0, initInfo: 0};
    newCamera.position = CameraPos();
    newCamera.quaternion = CameraOrientation();
    newCamera.initInfo = CameraInitInfo();
    //$scope.proc.cameraTrocar = $scope.proc.cameraTrocar || newCamera;
    $scope.proc.cameraSetup = newCamera; 
    if($scope.saveCamera)
      $scope.save();
  }

  $scope.addComments = function(stage){
	$uibModal.open({
      templateUrl: 'assets/partials/add-comments.html',
      controller: ['$scope','$uibModalInstance', function($scope,$uibModalInstance){
        var newComments = "";
        $scope.close = function(){ $uibModalInstance.dismiss(false); };
        $scope.add = function(){
			  stage.comments = newComments;
          if(stage.comments) {
            $uibModalInstance.close(true);
          }
        }

        $scope.newComments = function(v){
          if(typeof(v) == 'undefined')
            return  newComments;
          else {
            newComments = v;
			}
		}
        
      }]
    }).result.then(function(r){
      if(r) $scope.save();
    });
  }
  $scope.getIframeSrc = function(youtubeVideo) {
    return $sce.trustAsResourceUrl("https://youtube.com/embed/" + youtubeVideo.id + "?start=" + youtubeVideo.startTime + "&end=" + youtubeVideo.endTime);
  };

  $scope.setTrocarPlacement = function($event) {
    var x = $event.offsetX/$event.target.clientWidth, y = $event.offsetY/$event.target.clientHeight;
    var p = $scope.proc;
    p.cameraTrocar = p.cameraTrocar || { x: 0, y: 0};
    p.cameraTrocar.x = x;
    p.cameraTrocar.y = y;
    $scope.save();
  };
  $scope.shouldShowErrorsForTask = function(stageId, taskId){
    function touchedAndEmpty(x){ return x.$error.required; } // x.$touched
    var prefix = 'task' + stageId + '-' + taskId;
    return touchedAndEmpty(this.form[prefix + 'verb']) ||
      touchedAndEmpty(this.form[prefix + 'organ']) ||
      touchedAndEmpty(this.form[prefix + 'tool']);
  };
}]);

authorApp.controller('UserController', ['$scope','$rootScope', '$http', '$route', '$routeParams', 'Model', function($scope, $rootScope, $http, $route, $routeParams, Model){
  var model = Model.toLowerCase();
  var newModel, apiURL, modelID;

  function setID(id){
    newModel = id == 'new';
    apiURL = 'api/' + model + 's/' + (newModel ? '666' : id);
  }
  setID($routeParams.id);
  if(!newModel){
    $http.get(apiURL).success(function(data){
      if(data.isAdmin){
        data.isArtist = true; data.isAuthor = true;
      }
      $scope[model] = data;
    }).error(function(data){
      window.alert('Error: ' + data);
    });
  }

  function setEditable(user){
    if (user != null)
      $scope.isEditable = user.isAdmin;
  }
  setEditable($rootScope.activeUser);

  $scope.save = function save() {
    $http({ url: apiURL, data: this[model], method: 'PUT' }).success(function(data){
      if(newModel) setID(data.id);
    });
  }
}]);

authorApp.controller('QuizController', [ '$scope', '$rootScope', '$http', '$route', '$routeParams', 'confirmDelete', 'Model', function($scope, $rootScope, $http, $route, $routeParams, confirmDelete, Model) {
  var model = Model.toLowerCase();
  var newModel, apiURL, modelID;

  if($routeParams.id === 'undefined' || $routeParams.id === undefined) {
    $scope.displayQuizzes = true;
    apiURL = 'api/quizzes/';
    $scope.viewURL = '#/quizzes/';
    $scope.Model = Model;
    $http.get(apiURL).success(function(data) {
      $scope.quizzes = data.sort(function(a, b) { return a.name.localeCompare(b.name) });
    });
    $scope.remove = function($event, id, name) {
      $event.stopPropagation();
      confirmDelete(model + ' ' + name).result.then(function(result){
        $http.delete(apiURL + id).then( // Delete from DataStore
          function(success){
            removeById($scope.quizzes, id);
          },
          function(error){ window.alert('Error while deleting '+ model + ' ' + name + ' #'+ id); }
        )
      });
    }
    function setEditable(user){
      if (user != null) {
        $scope.isEditable = user.isAdmin || user.isAuthor;
      }
    }
    setEditable($rootScope.activeUser);
  } else {
    $scope.displayQuizzes = false;
    
    function setID(id) {
      newModel = id == 'new';
      apiURL = 'api/quizzes/' + (newModel ? '666' : id);
    }
    setID($routeParams.id);

    function setEditable(user) {
      if (user != null)
        $scope.isEditable = user.isAdmin || user.isArtist;
    }
    setEditable($rootScope.activeUser);

    $scope.save = function save() {
      $http({ url: apiURL, data: this[model], method: 'PUT' }).success(function(data) {
        if(newModel) setID(data.id);
      });
    }
  }

}]);

authorApp.controller('FaqsController', [ '$scope', '$rootScope', '$http', function($scope,$rootScope, $http) {
    }]);

authorApp.controller('ModelListController', [ '$scope', '$rootScope', '$http', '$location', 'confirmDelete', 'Model', function($scope,$rootScope, $http, $location, confirmDelete, Model) {

  var model = Model.toLowerCase();
  var apiURL = 'api/' + model + 's/';
  $scope.Model = Model;
  $scope.viewURL = '#/' + model + 's/';
  $http.get(apiURL).success(function(data){
    $scope.list = data.sort(function(a, b){return a.name.localeCompare(b.name)}); // sort based on name, not id
  });
// $http.delete('api/data/' + model + '/' + id + '/delete');  // Delete from BlobStore
  $scope.remove = function($event, id, name){
    $event.stopPropagation();
    confirmDelete(model + ' ' + name).result.then(function(result){
      $http.delete(apiURL + id).then( // Delete from DataStore
        function(success){
          removeById($scope.list, id);
        },
        function(error){ window.alert('Error while deleting '+ model + ' ' + name + ' #'+ id); }
      )
    });
  }

  function setEditable(user){
    if (user != null) {
      var opt0 = user.isAdmin;
      var opt1 = (model == 'tool' || model == 'organ') && user.isArtist;
      var opt2 = (model == 'procedure') && user.isAuthor;
      $scope.isEditable = opt0 || opt1 || opt2;
    }
  }
  setEditable($rootScope.activeUser);
  $scope.$on('ActiveUserIsSet', function(event, data) { // needed for 1st load since activeUser is initially null (data := activeUser)
      setEditable(data); // as soon as activeUser is set, update role flags (once per session)
  });

  $scope.open = function(id){
    $location.url('/' + model + 's/' + id);
  }
}]);

authorApp.controller('TrainController', ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
  $scope.procedureSelectButton = "Procedure";
  
  $http.get('api/procedures/').success(function(data) {
    $scope.list = data.sort(function(a, b) {
      return a.name.localeCompare(b.name);
    });
  });

  $scope.selectProcedure = function(proc_id, proc_name, proc_quiz) {
    $scope.procedureSelectButton = proc_name;
    $scope.selectProcedureId = proc_id;
    $scope.selectedProcedureName = proc_name;
    $scope.quizName = proc_quiz.name;
    $scope.quizUrl = proc_quiz.url;
  };
}]);

authorApp.controller('AuthBox', [ '$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location){
  $http.get('/api/auth').then(function(result){
    var user = result.data;
    $scope.loggedin = user.name != null;
    $rootScope.activeUser = user;
    $rootScope.$broadcast('ActiveUserIsSet', user);
    if(user.isAdmin || user.isAuthor || user.isArtist) {
      if($location.$$path == '/train') {
        $location.path('/procedures');
      }
    }
  });
}]);

authorApp.config(['$routeProvider',
  function($routeProvider){

    $routeProvider.when('/procedures/:id', {  // :id is a routeParam
      templateUrl: 'assets/partials/procedure.html',
      controller: 'ProcedureController'
    });
    $routeProvider.when('/organs/:id', {  // :id is a routeParam
      templateUrl: 'assets/partials/organ.html',
      controller: 'ToolOrganController',
      resolve: { Model: function(){ return 'Organ'; } },
    });
    $routeProvider.when('/tools/:id', { // :id is a routeParam
      templateUrl: 'assets/partials/tool.html',
      controller: 'ToolOrganController',
      resolve: { Model: function(){ return 'Tool'; } },
    });
    $routeProvider.when('/users/:id', { // :id is a routeParam
      templateUrl: 'assets/partials/user.html',
      controller: 'UserController',
      resolve: { Model: function(){ return 'User'; } },
    });

    $routeProvider.when('/quizzes', {
      templateUrl: 'assets/partials/quizzes.html',
      controller: 'QuizController',
      resolve: { Model: function() { return 'Quiz'; } }
    });
    $routeProvider.when('/quizzes/:id', {
      templateUrl: 'assets/partials/quizzes.html',
      controller: 'QuizController',
      resolve: { Model: function() { return 'Quiz'; } }
    });

    $routeProvider.when('/faqs', {
      templateUrl: 'assets/partials/faqs.html',
      controller: 'FaqsController',
    });

    $routeProvider.when('/train', {
      templateUrl: 'assets/partials/train.html',
      controller: 'TrainController'
    });

    ['Procedure', 'Organ', 'Tool', 'User'].forEach(function(Model){
      var model = Model.toLowerCase();
      var url = '/' + model + 's/';
      $routeProvider.when(url, {
        templateUrl: 'assets/partials/list-models.html',
        controller: 'ModelListController',
        resolve: { Model: function(){ return Model; } },
      });
    })

    $routeProvider.otherwise({ redirectTo: '/train' });
  }
]);

//$scope.proc : this access the procedure objs
