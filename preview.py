from models import *
from google.appengine.ext import blobstore
from zipfile import ZipFile
from io import BytesIO
from math import sqrt, cos, sin, pi, radians
import xml.etree.ElementTree as ET
import os
import logging
import codecs

from genscene import generateCamera, LogicalError

# Model conversion

def quads_to_triangles(quads):
    triangles = []
    for i in xrange(0, len(quads), 4): # convert quad_0123 to tri_012 and tri_230
        triangles += quads[i:i+3]
        triangles += quads[i+2:i+4]
        triangles += [quads[i]]
    return ' '.join(triangles)

def reorder_quad(quads):
    min_index, min_value = min(enumerate(quads), key=lambda i: i[1])
    quads = [quads[min_index], quads[(min_index+1)%4], quads[(min_index+2)%4], quads[(min_index+3)%4]]
    return ' '.join([str(i) for i in quads])

def hexahedra_to_quads(hexahedra):
    quad_faces = [[0,1,2,3],[4,7,6,5],[0,4,5,1],[1,5,6,2],[3,2,6,7],[0,3,7,4]]
    quads = set()
    for i in xrange(0, len(hexahedra), 8):
        for j in quad_faces:
            new_quad = reorder_quad([hexahedra[i+j[3]], hexahedra[i+j[2]], hexahedra[i+j[1]], hexahedra[i+j[0]]])
            rev_quad = reorder_quad([hexahedra[i+j[0]], hexahedra[i+j[1]], hexahedra[i+j[2]], hexahedra[i+j[3]]])
            if rev_quad in quads:
                quads.remove(rev_quad)
            else:
                quads.add(new_quad)
    quads = ' '.join(quads).split()
    return quads

def reorder_triangles(triangles):
    min_index, min_value = min(enumerate(triangles), key=lambda i: i[1])
    quads = [triangles[min_index], triangles[(min_index+1)%3], triangles[(min_index+2)%3]]
    return ' '.join([str(i) for i in triangles])

def tetrahedra_to_triangles(tetrahedra):
    triangle_faces = [[1,3,2], [0,2,3], [0,3,1], [0,1,2]]
    triangles = set()
    for i in xrange(0, len(tetrahedra), 4):
        for j in triangle_faces:
             new_triangle = reorder_triangles([tetrahedra[i+j[0]], tetrahedra[i+j[1]], tetrahedra[i+j[2]]])
             rev_triangle = reorder_triangles([tetrahedra[i+j[2]], tetrahedra[i+j[1]], tetrahedra[i+j[0]]])
             if rev_triangle in triangles:
                 triangles.remove(rev_triangle)
             else:
                 triangles.add(new_triangle)
    return ' '.join(triangles)

# Model generation
def generate_normals(positions, triangles, smooth_normals = True):
    normals = [0.0] * len(positions)
    for i in xrange(0, len(triangles), 3):
        a, b, c = triangles[i:i+3]
        A = positions[a*3:a*3+3]
        B = positions[b*3:b*3+3]
        C = positions[c*3:c*3+3]
        surface_normal = normalize(cross(subtract(B, A), subtract(C, A)))
        # smooth normals
        if smooth_normals:
          normals[a*3:a*3+3] = [x+y for x,y in zip(surface_normal, normals[a*3:a*3+3])]
          normals[b*3:b*3+3] = [x+y for x,y in zip(surface_normal, normals[b*3:b*3+3])]
          normals[c*3:c*3+3] = [x+y for x,y in zip(surface_normal, normals[c*3:c*3+3])]
        else:
          normals[a*3:a*3+3] = surface_normal
          normals[b*3:b*3+3] = surface_normal
          normals[c*3:c*3+3] = surface_normal
    for i in xrange(0, len(normals), 3):
        A = normals[i:i+3]
        normals[i:i+3] = normalize(A)
    return normals


def get_points(vertices, scale, rotate, translate):
    points = strings_to_points(vertices.split())
    scale_m = scale_matrix([float(v) for v in scale.split()])
    rotate_m = rotate_matrix([float(v) for v in rotate.split()])
    translate_m = translate_matrix([float(v) for v in translate.split()])
    transform_m = transform_matrix(scale_m, rotate_m, translate_m)
    return [transform_vector(p, transform_m) for p in points]


def bounding_sphere(points):
    # Ritter's bounding sphere
    x = points[0]
    y = max(points, key=lambda p: distance(p, x))
    z = max(points, key=lambda p: distance(p, y))

    center = midpoint(y,z)
    radius = distance(y,z)/2

    outside_points = [p for p in points if distance(p, center) > radius]
    for p in outside_points:
        if distance(p, center) > radius:
            radius = distance(p, center)

    return center, radius

# Data conversion

def strings_to_points(strings):
    values = [float(x) for x in strings]
    return zip(values[::3], values[1::3], values[2::3])

def points_to_string(points):
    return ' '.join([str(p) for p in points])

# vector operations

def cross(a, b):
    return [a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0]]

def subtract(a, b):
    return [a[0]-b[0], a[1]-b[1], a[2]-b[2]]

def add(a, b):
    return [a[0]+b[0], a[1]+b[1], a[2]+b[2]]

def normalize(a):
    magnitude = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2])
    if(magnitude != 0):
        return [a[0]/magnitude, a[1]/magnitude, a[2]/magnitude]
    return [0.0, 0.0, 1.0]

def distance(a, b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2 + (a[2]-b[2])**2)**.5

def midpoint(a, b):
    return [(a[0]+b[0])/2, (a[1]+b[1])/2, (a[2]+b[2])/2]

# 4x4 matix operations

def multiply_matrix(a, b):
    b_t = zip(*b)
    return [[sum(a_ij*b_ji for a_ij, b_ji in zip(a_i, b_j)) for b_j in b_t] for a_i in a]

def scale_matrix(s):
  return [[s[0], 0.0 , 0.0 , 0.0],
          [0.0 , s[1], 0.0 , 0.0],
          [0.0 , 0.0 , s[2], 0.0],
          [0.0 , 0.0 , 0.0 , 1.0]]

def translate_matrix(t):
  return [[1.0, 0.0, 0.0, 0.0],
          [0.0, 1.0, 0.0, 0.0],
          [0.0, 0.0, 1.0, 0.0],
          [t[0], t[1], t[2], 1.0 ]]

def rotate_matrix(r):
    cos_x = cos(radians(r[0]))
    sin_x = sin(radians(r[0]))
    mat_x = [[1.0,   0.0,    0.0, 0.0],
             [0.0, cos_x, sin_x, 0.0],
             [0.0, -sin_x,  cos_x, 0.0],
             [0.0,   0.0,    0.0, 1.0]]
    cos_y = cos(radians(r[1]))
    sin_y = sin(radians(r[1]))
    mat_y = [[ cos_y, 0.0, -sin_y, 0.0],
             [   0.0, 1.0,   0.0, 0.0],
             [sin_y, 0.0, cos_y, 0.0],
             [   0.0, 0.0,   0.0, 1.0]]
    cos_z = cos(radians(r[2]))
    sin_z = sin(radians(r[2]))
    mat_z =  [[cos_z, sin_z, 0.0, 0.0],
              [-sin_z,  cos_z, 0.0, 0.0],
              [  0.0,  0.0,   1.0, 0.0],
              [  0.0,  0.0,   0.0, 1.0]]
    return multiply_matrix(mat_x, multiply_matrix(mat_y, mat_z))

def transform_matrix(scale, rotate, translate):
    matrix = [[1.0, 0.0, 0.0, 0.0],
              [0.0, 1.0, 0.0, 0.0],
              [0.0, 0.0, 1.0, 0.0],
              [0.0, 0.0, 0.0, 1.0 ]]
    matrix = multiply_matrix(scale, matrix)
    matrix = multiply_matrix(rotate, matrix)
    matrix = multiply_matrix(translate, matrix)
    return matrix

def transform_vector(vector, transform):
    vector = [list(vector) + [1.0]]
    vector = multiply_matrix(vector, transform)
    return vector[0][:3]

# Parse data

def is_valid(data):
    return data is not None and len(data) > 0

def parse_model(model):
    res = None
    # OGLMODEL
    if model.tag == "OglModel":
        # VERTICES
        vertices = model.get('position')
        if is_valid(vertices):
            # INDICES
            indices = model.get('triangles')
            if model.get('quads') is not None:
                indices += quads_to_triangles(model.get('quads').split())

            if is_valid(indices):
                node = 'vertices="' + vertices + '" '
                node += 'indices="' + indices + '" '
                # NORMALS
                normals = model.get('normal')
                if not is_valid(normals):
                    vertices_f = [float(v) for v in vertices.split()]
                    indices_f = [int(i) for i in indices.split()]
                    normals = ' '.join([str(a) for a in generate_normals(vertices_f, indices_f)])
                node += 'normals="' + normals + '" '
                # MATERIAL
                material = model.get('material')
                if not is_valid(material):
                    material = 'Default Diffuse 1 0.5 0.5 0.5 1.0 Ambient 1 0.1 0.1 0.1 1.0 Specular 1 0.0 0.0 0.0 1 Emissive 1 0.0 0.0 0.0 1 Shininess 1 50'
                node += 'material="' + material + '" '
                # TRANSLATE
                translate = model.get('translation')
                if not is_valid(translate):
                    translate = '0.0 0.0 0.0'
                node += 'translate="' + translate + '" '
                # ROTATE
                rotate = model.get('rotation')
                if not is_valid(rotate):
                    rotate = '0.0 0.0 0.0'
                node += 'rotate="' + rotate + '" '
                # SCALE
                scale = model.get('scale')
                if not is_valid(scale):
                    scale = model.get('scale3d')
                    if not is_valid(scale):
                        scale = '1.0 1.0 1.0'
                node += 'scale="' + scale + '" '
                # BOUNDING SPHERE
                points = get_points(vertices, scale, rotate, translate)
                center, radius = bounding_sphere(points)
                node += 'center="' + points_to_string(center) + '" '
                node += 'radius="' + str(radius) + '" '

                node = '<PreviewModel ' + node + '/>'

                res = (node, (center, radius))
    return res

def parse_volume(volume, root):
    res = None
    # VERTICES
    vertices = volume.get('points')
    if is_valid(vertices):
        # INDICES
        indices = None
        if volume.tag == 'HexahedronSetTopologyContainer':
            indices = quads_to_triangles(hexahedra_to_quads(volume.get('hexahedra').split()))
        elif volume.tag == 'TetrahedronSetTopologyContainer':
            indices = tetrahedra_to_triangles(volume.get('tetrahedra').split())
        if is_valid(indices):
            node = 'vertices="' + vertices + '" '
            node += 'indices="' + indices + '" '
            # NORMALS
            normals = volume.get('normal')
            if not is_valid(normals):
                vertices_f = [float(v) for v in vertices.split()]
                indices_f = [int(i) for i in indices.split()]
                normals = ' '.join([str(a) for a in generate_normals(vertices_f, indices_f)])
            node += 'normals="' + normals + '" '
            # MATERIAL
            material = None
            for ogl in root.iter('OglModel'):
                material = ogl.get('material')
                if is_valid(material):
                    break
            if not is_valid(material):
                material = 'Default Diffuse 1 0.5 0.5 0.5 1.0 Ambient 1 0.1 0.1 0.1 1.0 Specular 1 0.0 0.0 0.0 1 Emissive 1 0.0 0.0 0.0 1 Shininess 1 50'
            node += 'material="' + material + '" '
            # TRANSFORM
            translate = None
            scale = None
            rotate = None
            for mo in root.iter('MechanicalObject'):
                translate = mo.get('translation')
                scale = mo.get('scale')
                rotate = mo.get('rotation')
                if not is_valid(scale):
                    scale = mo.get('scale3d')
                if is_valid(translate) or is_valid(scale) or is_valid(rotate):
                    break
            # TRANSLATE
            if not is_valid(translate):
                translate = '0.0 0.0 0.0'
            node += 'translate="' + translate + '" '
            # ROTATE
            if not is_valid(rotate):
                rotate = '0.0 0.0 0.0'
            node += 'rotate="' + rotate + '" '
            # SCALE
            if not is_valid(scale):
                scale = '1.0 1.0 1.0'
            node += 'scale="' + scale + '" '
            # BOUNDING SPHERE
            points = get_points(vertices, scale, rotate, translate)
            center, radius = bounding_sphere(points)
            node += 'center="' + points_to_string(center) + '" '
            node += 'radius="' + str(radius) + '" '

            node = '<PreviewModel ' + node + '/>'

            res = (node, (center, radius))
    return res

def parse_object(data, obj=None, mtl=None):
    nodes = ""
    root = ET.fromstring(data)
    spheres = []
    
    for node in root.findall('Node'):
        if node.get('name').lower().find('shaft') >= 0:
            root.remove(node)

    # OGLMODEL
    for model in root.iter('OglModel'):
        #Check if orgran is defined in OBJ file
        
        if ('fileMesh' in model.attrib or 'src' in model.attrib) and is_valid(obj):
            vertex, normal, triangleList = parse_objFile(obj)
            material = parse_mtlFile(mtl)
            # logging.info("Vertex : " + vertex)
            # logging.info("Normal : " + normal)
            # logging.info("Triangles : " + triangleList)
            model.set("position", vertex)
            if len(vertex) == len(normal) :
                model.set("normal", normal)
            model.set("triangles", triangleList)
            model.set("material", material)
        parsed = parse_model(model)

        if parsed is not None:
            nodes += parsed[0]
            spheres.append(parsed[1])

    # HEXAHEDRON
    for volume in root.iter('HexahedronSetTopologyContainer'):
        parsed = parse_volume(volume, root)
        if parsed is not None:
            nodes += parsed[0]
            spheres.append(parsed[1])

    # TETRAHEDRON
    for volume in root.iter('TetrahedronSetTopologyContainer'):
        parsed = parse_volume(volume, root)
        if parsed is not None:
            nodes += parsed[0]
            spheres.append(parsed[1])

    return nodes, spheres

def parse_objFile(objFile):
    vertexList = []
    textureList = []
    normalList = []
    triangleList = ""

    for line in objFile.split('\n'):
        split = line.split()
        #if blank line, skip
        if not len(split):
            continue
        if split[0] == "v":
            vertexList.append(split[1:])
        elif split[0] == "vt":
            textureList.append(split[1:])
        elif split[0] == "vn":
            normalList.append(split[1:])
        elif split[0] == "f":
            temp = split[1:]
            for t in temp:
                triangleList += str(int(t.split("/")[0])-1) 
                triangleList += " "

    vertex = ""
    normal = ""
    for v in vertexList:
        vertex += " ".join(v)
        vertex += " "
    for n in normalList:
        normal += " ".join(n)    
        normal += " "

    return vertex, normal, triangleList

"""
    Parsing MTL file for material information when object is defined in OBJ file instead of explicit defination in Organ XML
    Input:
    Ka 1.000000 1.000000 1.000000 ambient
    Kd 0.640000 0.640000 0.640000 diffusion
    Ks 0.500000 0.500000 0.500000 spectrum
    Ke 0.000000 0.000000 0.000000 emission 

    Output:
    Default Diffuse 1 0.64000004529953 0.64000004529953 0.64000004529953  1.0 
    Ambient 1 0.800000011920929 0.800000011920929 0.800000011920929  1 
    Specular 1 0.5 0.5 0.5  1 
    Emissive 1 0.0 0.0 0.0  1 
    Shininess 1 50 
"""

def parse_mtlFile(mtlFile):
    diffuse = ""
    ambient = ""
    specular = ""
    emissive = ""
    shininess = " Shininess 1 50"
    for line in mtlFile.split('\n'):
        split = line.split()
        if not len(split):
            continue
        if split[0] == "Ka":
            ambient = " Ambient 1 " + " ".join(split[1:]) + " 1 "
        elif split[0] == "Kd":
            diffuse = " Diffuse 1 " + " ".join(split[1:]) + " 1.0 "
        elif split[0] == "Ks":
            specular = " Specular 1 " + " ".join(split[1:]) + " 1 "
        elif split[0] == "Ke":
            emissive = " Emissive 1 " + " ".join(split[1:]) + " 1 "
        
    return "Default" + diffuse + ambient + specular + shininess
    # return "Default Diffuse 1 0.262745 0.207843 0.043137 1.0 Ambient 1 0.200000 0.200000 0.200000 1 Specular 1 1.000000 1.000000 1.000000 1 Shininess 1 50"
# Generate previews

def generate_bounding_sphere(spheres, average = False):
    new_center, new_radius = spheres[0]
    # LARGEST BOUNDING BOX
    if len(spheres) > 1 and not average:
        points = []
        for c, r in spheres:
            points.append((c[0] + r, c[1], c[2]))
            points.append((c[0] - r, c[1], c[2]))
            points.append((c[0], c[1] + r, c[2]))
            points.append((c[0], c[1] - r, c[2]))
            points.append((c[0], c[1], c[2] + r))
            points.append((c[0], c[1], c[2] - r))

        new_center, new_radius = bounding_sphere(points)
    # AVERAGE BOUNDING BOX
    elif len(spheres) > 1 and average:
        centers = [s[0] for s in spheres]
        radii = [s[1] for s in spheres]
        new_center = [sum(c)/len(c) for c in zip(*centers)]
        new_radius = sum(radii)/len(radii)

    center_str = ' '.join([str(fl) for fl in new_center])
    radius_str = str(new_radius)

    node = 'center="' + center_str + '" '
    node += 'radius="' + radius_str + '" '
    node = '<PreviewBound ' + node + '/>'
    return node

def generate_object_preview(object, objectFile=None, mtlFile=None):
    nodes, spheres = parse_object(object, objectFile, mtlFile)
    nodes += generate_bounding_sphere(spheres)
    node = '<PreviewObject> ' + nodes + '</PreviewObject>'
    return node

def generate_scene_preview(procedure):
  # TODO: Add more comments
  # First parse the procedure and get a list
  # of organs and tools for the procedure
  # these should be methods of Procedure itself
  # Find the right candidate for all organs
  organs = procedure.objects_used(Organ)

  # at this stage we might throw error if something does
  # not have a candidate
  for o in organs:
    if organs[o]['candidate'] == None:
      raise LogicalError("Organ '%s' does not have a candidate that supports %s" % (o,listify(organs[o]['verbs'])))

  s = BytesIO()
  z = ZipFile(s, 'w')

  def add_candidate_data(c):
    d = blobstore.get(c.data)
    _, ext = os.path.splitext(d.filename)
    if ext.lower() == '.xml':
      r = blobstore.BlobReader(d.key())
      x = parse_object(r.read(),)
    elif ext.lower() == '.zip':
      r = blobstore.BlobReader(d.key())
      # unzip it and add all the files
      # to the current ZIP in a new directory called dn
      z = ZipFile(r, 'r')
      xmlFile = None
      objFile = None
      mtlFile = None

      for f in z.namelist():
        _, ext = os.path.splitext(f)
        if ext.lower() == '.xml' and f.find("MACOSX") < 0:
            xmlFile = z.open(f).read()
        if ext.lower() == '.obj' and f.find("MACOSX") < 0:
            objFile = z.open(f).read()
        if ext.lower() == '.mtl' and f.find("MACOSX") < 0:
            mtlFile = z.open(f).read()
      if xmlFile is not None:
          x = parse_object(xmlFile, objFile, mtlFile)
      z.close()
    else:
      raise LogicalError("Unknown file extension '%s'" % d.filename)
    return x

  organ_candidates = []
  for organ in organs.values():
    organ_candidates.append(organ['candidate'])
    organ_candidates.extend(organ['related'])

  organs_keymap = dict([ (o.key, o) for o in organ_candidates ])

  organ_xml = map(add_candidate_data, organs_keymap.values())
  organ_previews = [organ[0] for organ in organ_xml] 
  spheres = []
  for organ in organ_xml:
    if len(organ[1]) > 0:
        spheres.append(organ[1][0])

  bounds = generate_bounding_sphere(spheres)

  xml = '<PreviewScene>' +  ''.join(organ_previews) + bounds + '</PreviewScene>'

  return xml