# Introduction

TIPS Author is a cloud-based web application that is intended to act as a front-end/hub for TIPS authoring, administration and subsequent usage.

# Technologies Stack

* `Google App Engine` is the platform that the server-side runs on. It provides both hosting and a framework to interact with the hosting environment.
It includes a database and easy to configure web-server. All of it accessed through a Python API. The configuration for
Google App Engine in done through app.yaml. download link:https://cloud.google.com/appengine/docs/standard/python/download#appengine_sdk

* `Single-page web-application` is a design pattern in which the web-application only has one HTML page in index.html and 
all the other parts of the application and data is loaded using Javascript. The entire website has only one static HTML file.

* `AngularJS` the Javascript web-application framework we use on the client side. It takes care of many details involving
updating the page and creating GUI and handling requests to server and back.

* `Javascript REST API` is a design pattern used to make writing application more consistent. That for every object there is 4 different
methods: Create, Read, Update, Delete. All the interaction between client(Javascript) and server (Python) is done through API end-points
and the data is formatted as JSON.

* `Mercurial (hg)` is the version control system used for this project in BitBucket.


For more information on the various frameworks/libraries, links are provided below:

* [Google App Engine](https://cloud.google.com/appengine/docs/python/)
* [AngularJS](https://angularjs.org/)
* [JavaScript REST API](http://developer.marklogic.com/try/rest/index)
* [Mercurial](https://www.mercurial-scm.org/guide)

# Server Side Code

* `app.yaml`: This is the configuration file used by the Google App Engine defining how the web server should work. Application name is the same as the URL on appspot.com. In this case it is tips-author.appspot.com. The version
is chosen by the user it allows you to upload multiple version of the same application to online. You can access 
specific versions of the tips-author by going to 3.tips-author.appspot.com

Then there are the handlers that define mapping between URLs and Python code::

	url: /assets
	static_dir: assets

This maps anything under URL /assets to the source directory assets. This is used to access all the Javascript and

CSS code::

	url: /
    static_files: index.html
    upload: index.html
  
This tells the web server to serve index.html for the homepage::

    url: /api/.*
    script: api.app
  
This says that anything under /api is handle dynamically by api.app. That is a Python reference which means
the app variable inside the api module (api.py)::

    libraries:
      name: webapp2
      version: "2.5.2"
  
List of the Python libraries that we are using. There are only a small set of libraries supported::

    skip_files: 
    ....

This is a list of patterns of files that should not be uploaded to the web-server

To upload you run this from command-line::

    appcfg update app.yaml

appcfg is a command-line utility from `Google App Engine SDK <https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python>`

* `api.py` defines all the API end-points of the TIPS-Author application. It consists of many little Python functions that serve the data from the database to the Javascript application.
It is modeled after Javascript REST APIs. Almost all of the handlers just do a simple task and return some 
JSON data as the result.

* `genscene.py` puts together XML files for different organs and tools and generates a scene. The scene is index.scn and is generated on
the fly by starting from the template (xml/tempalte.scn) and adding the proper include tags for all the mentioned organs and tools
in there. All of the XML files, dependent files and index.scn are put together into a zip file that is returned to the user.

* `models.py` defines all the database objects and how they are accessed. It also contains some methods that do some data conversion.

* `preview.py` parses the scene from the XML .scn file provided during procedure creation and generates and returns a temporary xml file which can be used to generate a WebGL preview of the scene on the client side.

# Client Side Code

* `index.html` contains the overall template for the web pages. The top level nav-bar and account information resides here. The main content changes based on the link/tab that is currently selected. The html for these are in `assets\partials` and html files correspond to the navigation links that appear on the web application.

* `author-app.js` is the main javascript file that contains the module/controller code as per the AngularJS framework. Any data input to a form or any data to be loaded in a page from a web service is first processed here in the appropriate controller function.

* `fx.js` contains WebGL functions used for generating preview of the scene for a procedure.

* `readfile.js` reads and parses the temporary xml generated by `preview.py` and returns an array of objects and their bounds.

# Code Flow

Most interactions made on the front-end web page go through the following flow in the code:
* A user clicks on a button or a link on the page.
* The appropriate controller for the action is determined in `author-app.js`. In the controller depending on the user action, the appropriate function/code is executed.
* Such a function/code often involved calling a web service endpoint defined in `api.py` to either post/get some data. In `api.py` an appropriate python method is called depending on the route provided in the javascript function. This function returns data/status back to the caller javascript function which in turn sets this value to an angular `scope variable`.
* The `scope variable` is then accessed in the html code which is used to display data to the user on the web page.

## Sample Code Flow

### User selects a procedure

# TIPS Author on Google App Engine

Upload to GAE like this:

    appcfg.py update app.yaml

Run a local webserver like this

   dev_webserver .
   
