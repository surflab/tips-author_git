
Techonologies used
==================
* `Google App Engine`_: is the platform that the server-side runs on. It provides both hosting and a framework to interact with the hosting environment.
It includes a database and easy to configure web-server. All of it accessed through a Python API. The configuration for
Google App Engine in done through app.yaml.

* `Single-page web-application`: it is a design pattern in which the web-application only has one HTML page in index.html and 
all the other parts of the application and data is loaded using Javascript. The entire website has only one static HTML file.

* `AngularJS`_: is the Javascript web-application framework we use on the client side. It takes care of many details involving
updating the page and creating GUI and handling requests to server and back.

* `Javascript REST API`_: is a design pattern used to make writing application more consistent. That for every object there is 4 different
methods: Create, Read, Update, Delete. All the interaction between client(Javascript) and server (Python) is done through API end-points
and the data is formatted as JSON.


.. _Google App Engine: https://cloud.google.com/appengine/docs/python/

.. _AngularJS: https://angularjs.org/

.. _Javascript REST API: http://developer.marklogic.com/try/rest/index

Server-side code
================

app.yaml
--------
It is the configuration for how the web-server should work. Some parameters are exactly the same as
they were in the sample::

    runtime:python27
    api_version: 1
    threadsafe: yes

Some I have changed because I had to::

    application: tips-author
    version: 3

Application name is the same as the URL on appspot.com. In this case it is tips-author.appspot.com. The version
is chosen by the user it allows you to upload multiple version of the same application to online. You can access 
specific versions of the tips-author by going to 3.tips-author.appspot.com

Then there are the handlers that define mapping between URLs and Python code::

    - url: /assets
      static_dir: assets

This maps anything under URL /assets to the source directory assets. This is used to access all the Javascript and
CSS code::

    - url: /
      static_files: index.html
      upload: index.html
  
This tells the web server to serve index.html for the homepage::

    - url: /api/.*
      script: api.app
  
This says that anything under /api is handle dynamically by api.app. That is a Python reference which means
the app variable inside the api module (api.py)::

    libraries:
    - name: webapp2
      version: "2.5.2"
  
List of the Python libraries that we are using. There are only a small set of libraries supported::

    skip_files: 
    ....

This is a list of patterns of files that should not be uploaded to the web-server

To upload you run this from command-line::

    appcfg update app.yaml

appcfg is a command-line utility from `Google App Engine SDK <https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python>`_

api.py
------
all API end-points of the TIPS-Author application. 
Consists of many little Python functions that serve the data from the database to the Javascript application.
It is modeled after Javascript REST APIs. Almost all of the handlers just do a simple task and return some 
JSON data as the result.


genscene.py
-----------

Puts together XML files for different organs and tools and generates a scene. The scene is index.scn and is generated on
the fly by starting from the template (xml/tempalte.scn) and adding the proper include tags for all the mentioned organs and tools
in there. All of the XML files, dependent files and index.scn are put together into a zip file that is returned to the user.

models.py
---------

Defines all the database objects and how they are accessed. It also contains some methods that do some data conversion.
